package com.elka.integralreader.provider.response;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.util.Log;

import com.elka.integralreader.provider.ContentsTable;
import com.elka.integralreader.provider.FileHandlerFactory;
import com.elka.integralreader.utils.FileManager;

public class ArticleResponseHandler extends DataProcessor {

	private final String mLogSs = "ArticleResponseHandler";
    private static final String htmlHeader = "<html><head></head><body style=\"text-align:justify;color:black;background-color:#faeedd;\">";
    private static final String htmlFooter = "</body></html>";
    private final ContentProvider provider;
    
    public ArticleResponseHandler(ContentProvider provider) {
        this.provider = provider;
    }
    
	/**
	 *  Need to parse the articles page and add into the database.
	 *  Can make insert into the ContentProvider giving it a ContentValues set.
	 */
	@Override
	public boolean parseDataEntity(Document doc) {
		Log.i(mLogSs, "Entering parseDataEntity");
       
		File file = FileHandlerFactory.getFileForName(getLocalUri().getLastPathSegment() + getLocalUri().hashCode());
        
		Element content = doc.getElementById("content");
		if (content != null) {
			Log.i(mLogSs, "Found content id");
			Elements elems = content.getElementsByTag("div");
			Log.i(mLogSs, "Found " + elems.size() + " div tags in content");
			
			for (Element elem : elems) {
				if (elem.className().isEmpty() && elem.id().isEmpty() && elem.hasText()) {
					Log.i(mLogSs, "Looks like we've found the content, writing the file");
					Log.i(mLogSs, elem.html());
					FileOutputStream fileStream;
                    try {
                        fileStream = new FileOutputStream(file, false);
						//fileStream.write(htmlHeader.getBytes());
						fileStream.write(elem.html().getBytes());	// FIXME: need to make this one fs call, using Element.wrap()
																	// it's crashing with IndexOutOfBounds exception now
						//fileStream.write(htmlFooter.getBytes());
						fileStream.close();
						FileManager.put(file.getAbsolutePath(), elem.html());

						ContentValues values = new ContentValues(1);
						values.put(ContentsTable.COLUMN_FILE, file.getAbsolutePath());
						String selection = ContentsTable.COLUMN_URI + " = '" + getPath() + "'";
						provider.update(getLocalUri(), values, selection, null);
                    } catch (IOException e) {
                        Log.e("ArticleResponseHandler", "got IOException");
                        e.printStackTrace();
                    }
				}
			}
		}

        Log.i(mLogSs, "Done");

		return true;
	}

	@Override
	public void deleteOld() {
		// TODO Auto-generated method stub
		
	}
}
