package com.elka.integralreader.provider.response;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.elka.integralreader.provider.NormalizedUri;

import android.net.Uri;
import android.util.Log;

public abstract class DataProcessor { 
    private NormalizedUri uri;
    public void execute(NormalizedUri uri) {
        this.uri = uri;
        Document doc;
        try {
            Log.i("DataProcessor", "Connecting to " + uri.getQueryUri().toString());
            doc = Jsoup.connect(uri.getQueryUri().toString()).timeout(60000).get();
            Log.i("DataProcessor", "Read the doc " + doc.title());
            if (parseDataEntity(doc)) {
                deleteOld();
            }
        } catch (IOException e) {
            Log.i("DataProcessor", "Got IOException" + e.getLocalizedMessage() + e.getMessage());
            e.printStackTrace();
        } 
    }
    public Uri getLocalUri() { return uri.getLocalUri(); }
    public String getPath() { return uri.getPath(); }
	public abstract boolean parseDataEntity(Document doc);
	public abstract void deleteOld();
}
