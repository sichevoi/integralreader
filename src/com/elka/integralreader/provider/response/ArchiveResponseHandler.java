package com.elka.integralreader.provider.response;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.elka.integralreader.provider.ContentsTable;
import com.elka.integralreader.utils.ILRTypes.MediaType;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

public class ArchiveResponseHandler extends DataProcessor {
    private final static String mLogSs = "ArchiveResponseHandler";
    private final static String VIEW_CLASS = "view-content";
    private final static String VIEWS_ROW = "views-row ";
    private final ContentProvider provider; 
    public ArchiveResponseHandler(ContentProvider provider) {
        this.provider = provider;
    }
    
    @Override
    public boolean parseDataEntity(Document doc) {
        Element content = doc.getElementById("content");
        Elements viewClass = content.getElementsByClass(VIEW_CLASS);
        if (!viewClass.isEmpty()) {
            Log.i(mLogSs, "Found class view-content, size = " + viewClass.size());
            Element view = viewClass.get(0);
            Elements viewsRows = view.getElementsByAttributeValueStarting("class", VIEWS_ROW);
            if (!viewsRows.isEmpty()) {
                Log.i(mLogSs, "Found " + viewsRows.size() + " view rows");
                for (Element elem : viewsRows) {
                    ContentValues entry = new ContentValues();
                    entry.put(ContentsTable.COLUMN_THUMB, getImagePath(elem));
                    entry.put(ContentsTable.COLUMN_URI, getUrl(elem));
                    entry.put(ContentsTable.COLUMN_TITLE, getTitle(elem));
                    entry.put(ContentsTable.COLUMN_CONTRIB, getContributors(elem).get(0));
                    entry.put(ContentsTable.COLUMN_ABSTRACT, getAbstract(elem));
                    entry.put(ContentsTable.COLUMN_TYPE, getType(elem).getInt());
                    try {
                        provider.insert(getLocalUri(), entry);
                    } catch (SQLiteConstraintException e) {
                        Log.i(mLogSs, "Got constraint exception, exitting");
                        break;
                    }
                }
            }
        } else {
            Log.i(mLogSs, "Cannot get element with class view-content");
        }
        return false;
    }

    private final static String FIELD_IMAGE_FID = "views-field-field-image-fid";
    private String getImagePath(Element elem) {
        String imageHref = null;
        Elements image = elem.getElementsByClass(FIELD_IMAGE_FID);
        if (!image.isEmpty()) {
            Elements image_tag = image.get(0).getElementsByTag("img");
            if (!image_tag.isEmpty()) {
                imageHref = image_tag.attr("src");
                Log.i(mLogSs, "Found image path: " + imageHref);
            }
        }
        return imageHref;
    }
    
    private final static String VIEWS_FIELD_TITLE = "views-field-title";
    private String getUrl(Element elem) {
        String url = null;
        Elements titleFields = elem.getElementsByClass(VIEWS_FIELD_TITLE);
        if(!titleFields.isEmpty()) {
            Elements a_tag = titleFields.get(0).getElementsByTag("a");
            if (!a_tag.isEmpty()) {
                url = a_tag.attr("href");
                Log.i(mLogSs, "Found url: " + url);
            }
        }
        return url;
    }
    
    private String getTitle(Element elem) {
        String title = null;
        Elements titleFields = elem.getElementsByClass(VIEWS_FIELD_TITLE);
        if(!titleFields.isEmpty()) {
            Elements titleTag = titleFields.get(0).getElementsByTag("a");
            if (!titleTag.isEmpty()) {
                title = titleTag.text();
                Log.i(mLogSs, "Found title: " + title);
            }
        }
        return title;
    }
    
    private final static String FIELD_CONTRIBUTORS = "views-field-field-contributor-nid";
    private List<String> getContributors(Element elem) {
        List<String> contribs = new ArrayList<String>(4);
        Elements elems = elem.getElementsByClass(FIELD_CONTRIBUTORS);
        if (!elems.isEmpty()) {
            Elements names = elems.get(0).getElementsByTag("a");
            for (Element name : names) {
                if (name.hasText()) {
                    contribs.add(name.text());
                    Log.i(mLogSs, "Found contributor: " + name.text());
                }
            }
        }
        
        if(contribs.size() == 0) {
            contribs.add("");
        }
        return contribs;
    }
    
    private final static String FIELD_ABSTRACT = "views-field-field-abstract-value";
    private String getAbstract(Element elem) {
        String abstr = null;
        Elements elems = elem.getElementsByClass(FIELD_ABSTRACT);
        if (!elems.isEmpty()) {
            Elements p_tags = elems.get(0).getElementsByTag("p");
            if (!p_tags.isEmpty()) {
                abstr = p_tags.get(0).text();
                Log.i(mLogSs, "Found abstract: " + abstr);
            }
        }
        return abstr;
    }
    
    private final static String FIELD_LABEL_1 = "views-field-tid-1";
    private MediaType getType(Element elem) {
        MediaType type = MediaType.UNKNOWN;
        Elements elems = elem.getElementsByClass(FIELD_LABEL_1);
        if (!elems.isEmpty()) {
            Elements a_tag = elems.get(0).getElementsByTag("a");
            if(!a_tag.isEmpty()) {
                String typeName = a_tag.get(0).text();
                if (typeName.equalsIgnoreCase("Article")) {
                    type = MediaType.ARTICLE;
                } else if (typeName.equalsIgnoreCase("Video")) {
                    type = MediaType.VIDEO;
                } else if (typeName.equalsIgnoreCase("Art Gallery")) {
                    type = MediaType.ART_GALLERY;
                } else if (typeName.equalsIgnoreCase("Audio")) {
                    type = MediaType.AUDIO;
                } 
            }
        }
        return type;
    }
    @Override
    public void deleteOld() {
        // TODO Auto-generated method stub
        
    }
}
