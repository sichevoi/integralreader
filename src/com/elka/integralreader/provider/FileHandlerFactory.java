package com.elka.integralreader.provider;

import java.io.File;

public enum FileHandlerFactory {
	INSTANCE;

	private static String cacheDir;
	
	public static void setCacheDir(String cacheDir)
	{
		FileHandlerFactory.cacheDir = cacheDir;
		File dir = new File(cacheDir);
		if (!dir.exists()){
			dir.mkdir();
		}
	}
	
	public static FileHandler getFileForId(String id) {
		return new FileHandler(cacheDir, id);
	}

	public static File getFileForName(String name) {
		return new File(cacheDir + "/" + name);
	}
}
