package com.elka.integralreader.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public abstract class ContentsTable implements BaseColumns {
	public static final String TABLE_NAME = "contents";

	public static final String AUTHORITY = "com.elka.integralreader.provider";
	
	static final Uri CONTENTS_URI = Uri.parse("content://" + AUTHORITY);

	// database columns - all text
	public static final String COLUMN_TYPE 		= "type";
	public static final String COLUMN_URI       = "uri";
	public static final String COLUMN_TITLE 	= "title";
	public static final String COLUMN_CONTRIB 	= "contributors";
	public static final String COLUMN_YEAR		= "year";
	public static final String COLUMN_MONTH 	= "month";
	public static final String COLUMN_THUMB 	= "thumbpath";
	public static final String COLUMN_ABSTRACT 	= "abstract";
	public static final String COLUMN_FILE 		= "filepath";
}
