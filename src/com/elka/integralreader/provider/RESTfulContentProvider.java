/**
 * 
 */
package com.elka.integralreader.provider;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;

import com.elka.integralreader.provider.response.DataProcessor;

import android.content.ContentProvider;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author homyak
 * @param <UriRequestTask>
 *
 */
public abstract class RESTfulContentProvider extends ContentProvider {
	
	private final Map<String, UriRequestTask> mRequestsInProgress = new HashMap<String, UriRequestTask>();
	
	private UriRequestTask getRequestTask(String queryText)
	{
		return mRequestsInProgress.get(queryText);
	}
	
	/**
	* Allows the subclass to define the database used by a response handler
	* @return database passed to response handler.
	*/
	public abstract SQLiteDatabase getWritableDatabase();
	public abstract SQLiteDatabase getReadableDatabase();
	
	public void requestComplete (String queryText)
	{
		synchronized (mRequestsInProgress)
		{
			//mRequestsInProgress.remove(queryText);
		}
	}
	
	/**
	* Abstract method that allows a subclass to define the type of handler
	* that should be used to parse the response of a given request.
	 * @param <T>
	*
	* @param requestTag unique tag identifying this request.
	* @return The response handler created by a subclass used to parse the
	* request response.
	*/
	protected abstract DataProcessor newResponseHandler(String requestTag, NormalizedUri uri);
	
	UriRequestTask newQueryTask (String requestTag, NormalizedUri queryUri ) {
		UriRequestTask requestTask;
		
		final HttpGet getter = new HttpGet(queryUri.getQueryUri().toString());
		
		// TODO: verify we really need the generic type here
		DataProcessor handler = newResponseHandler(requestTag, queryUri);
		requestTask = new UriRequestTask(requestTag, this, getter, handler, getContext());
		
		mRequestsInProgress.put(requestTag, requestTask);
		
		return requestTask;
	}

    /**
	* Creates a new worker thread to carry out a RESTful network invocation.
	* @param queryTag unique tag that identifies this request.
	* @param queryUri the complete URI that should be accessed by this request.
	*/
	public void asynchQueryRequest(String queryTag, NormalizedUri queryUri) {
		synchronized (mRequestsInProgress)
		{
			UriRequestTask requestTask = getRequestTask(queryTag);
			if (requestTask == null)
			{
				requestTask = newQueryTask(queryTag, queryUri);
				Thread t = new Thread(requestTask);
				// allows other requests to run in parallel
				t.start();
			}
		}
	}
}
