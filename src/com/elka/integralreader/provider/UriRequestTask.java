package com.elka.integralreader.provider;

import org.apache.http.client.methods.HttpUriRequest;
import com.elka.integralreader.provider.response.DataProcessor;

import android.content.Context;

public class UriRequestTask implements Runnable {
	
	private final static String mLogSs = UriRequestTask.class.getSimpleName();
	
	private HttpUriRequest mRequest;
	private DataProcessor mHandler;
	protected Context mAppContext;
	private RESTfulContentProvider mSiteProvider;
	private String mRequestTag;
	
	public UriRequestTask(HttpUriRequest request, 
	                      DataProcessor handler, 
						  Context appContext)
	{
		this(null, null, request, handler, appContext);
	}
	
	public UriRequestTask(String requestTag, 
						  RESTfulContentProvider siteProvider,
						  HttpUriRequest request,
						  DataProcessor handler,
						  Context appContext)
	{
		mRequestTag = requestTag;
		mSiteProvider = siteProvider;
		mRequest = request;
		mHandler = handler;
		mAppContext = appContext;
	}
	
	public void setRawResponse(int rawResponse)
	{
	}
	
	/**
	* Carries out the request on the complete URI as indicated by the protocol,
	* host, and port contained in the configuration, and the URI supplied to
	* the constructor.
	*/
	@Override
	public void run() {
		mHandler.execute(getUri());
		if (mSiteProvider != null)
		{
			mSiteProvider.requestComplete(mRequestTag);
		}
	}

	private NormalizedUri getUri() {
		return NormalizedUri.fromJavaNetURI(mRequest.getURI());
	}
}
