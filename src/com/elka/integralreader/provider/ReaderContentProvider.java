package com.elka.integralreader.provider;

import java.io.FileNotFoundException;

import com.elka.integralreader.provider.response.ArchiveResponseHandler;
import com.elka.integralreader.provider.response.DataProcessor;
import com.elka.integralreader.provider.response.ArticleResponseHandler;
import com.elka.integralreader.provider.response.PageResponseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

/**
 * @author homyak
 * ReaderContentProvider - content provider for the app.
 * Manages internal cache in an SQL database. Maintains
 * the database up to date.
 * 
 * http://integrallife.com/
 * 
 */
public final class ReaderContentProvider extends RESTfulContentProvider {
	private static final String tag = ReaderContentProvider.class.getSimpleName();
	
	private static final String DATABASE_NAME =  ContentsTable.TABLE_NAME + ".db";
	private static final String FILE_CACHE_DIR =
            "/data/data/com.elka.integralreader/file_cache";
	
	private static final String IL_AUTHORITY = "integrallife.com";
	// TODO: make this https?
	static final Uri QUERY_URI = Uri.parse("http://integrallife.com/");

	public enum ContentType
	{
		PAGE,
		TEXT,
		AUDIO,
		VIDEO,
		UNKNOWN
	}
	
	private static final int VIDEO_ID 		= 0;
	private static final int INTEGRAL_POST 	= 1;
	private static final int AUDIO_ID		= 2;
	private static final int ARCHIVE		= 3;
	
	private static UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	
	static {
		// TODO: add other URIs here - contributors, channels, etc.

		sUriMatcher.addURI(ContentsTable.AUTHORITY, "video/*", VIDEO_ID);
		sUriMatcher.addURI(ContentsTable.AUTHORITY, "integral-post/*", INTEGRAL_POST);
		sUriMatcher.addURI(ContentsTable.AUTHORITY, "audio/*", AUDIO_ID);
		sUriMatcher.addURI(ContentsTable.AUTHORITY, "archive/*", ARCHIVE);
		sUriMatcher.addURI(ContentsTable.AUTHORITY, "all-content", ARCHIVE);
		
		sUriMatcher.addURI(IL_AUTHORITY, "video/*", VIDEO_ID);
		sUriMatcher.addURI(IL_AUTHORITY, "integral-post/*", INTEGRAL_POST);
		sUriMatcher.addURI(IL_AUTHORITY, "audio/*", AUDIO_ID);
		sUriMatcher.addURI(IL_AUTHORITY, "archive/*", ARCHIVE);	
		sUriMatcher.addURI(IL_AUTHORITY, "all-content", ARCHIVE);
	}
	
	private DatabaseHelper mOpenHelper;
	private SQLiteDatabase writableDatabase;
	private SQLiteDatabase readableDatabase;
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
	    static int DATABASE_VERSION = 9;

		public DatabaseHelper(Context context, String name,
				CursorFactory factory) {
			super(context, name, factory, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			createTable(db);
		}

		private void createTable(SQLiteDatabase db) {
			String createContentTable = 
					"CREATE TABLE " + ContentsTable.TABLE_NAME + " (" + 
					ContentsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
					ContentsTable.COLUMN_TYPE + " INT, " + 
					ContentsTable.COLUMN_TITLE + " TEXT NOT NULL, " + 
					ContentsTable.COLUMN_CONTRIB + " TEXT, " + 
					ContentsTable.COLUMN_THUMB + " TEXT, " + 
					ContentsTable.COLUMN_YEAR + " INT, " +
					ContentsTable.COLUMN_MONTH + " INT, " +
					ContentsTable.COLUMN_ABSTRACT + " TEXT, " +
					ContentsTable.COLUMN_FILE + " TEXT, " +
					ContentsTable.COLUMN_URI + " TEXT NOT NULL UNIQUE" +
					");";
			db.execSQL(createContentTable);
			Log.i("DatabaseHelper", "Created a database: " + createContentTable);
		}
		
        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldv,
                              int newv)
        {
            Log.i(tag, "Upgrading the database");
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +
            		ContentsTable.TABLE_NAME + ";");
            createTable(sqLiteDatabase);
        }
	}
	
	public ReaderContentProvider() {
		FileHandlerFactory.setCacheDir(FILE_CACHE_DIR);
	}

	private final String ALL_CONTENT_PATH = "/all-content";
	private void updateItemsList() {
	    Log.i(tag, "Updating itemsList");
	    NormalizedUri uri = new NormalizedUri(ALL_CONTENT_PATH);
        asynchQueryRequest(uri.getPath(), uri);
        final String selection = "LIMIT 1";
        Cursor queryCursor = getReadableDatabase().query(false, ContentsTable.TABLE_NAME, 
                null, 
                null, 
                null, 
                null, 
                null,
                null,
                "1");
        if (queryCursor.getCount() != 0) {
            queryCursor.moveToFirst();
            String lastItem = queryCursor.getString(queryCursor.getColumnIndex(ContentsTable.COLUMN_URI));
            Log.i(tag, "Last item in the database: " + lastItem);
        }
	}
	
	@Override
	public SQLiteDatabase getWritableDatabase() {
		return writableDatabase;
	}

	@Override
	public SQLiteDatabase getReadableDatabase() {
	    return readableDatabase;
	}

	// The response handler depends on the content type.
	// Need to create different handlers for different types.
	@Override
	protected DataProcessor newResponseHandler(String requestTag, NormalizedUri uri) {
		Log.i(tag, "Matching uri " + uri.getPath());
		int id = sUriMatcher.match(uri.getLocalUri());
		DataProcessor rsp = null;
		Log.i(tag, "Matched id = " + id);
		switch (id) {
		case INTEGRAL_POST:
			Log.i(tag, "Matched integral-post tag");
			rsp = new ArticleResponseHandler(this);
			break;
		case ARCHIVE:
		    Log.i(tag, "Matched archive tag");
		    rsp = new ArchiveResponseHandler(this);
		    break;
		default:
			Log.i(tag, "Matched default tag");
			rsp = new PageResponseHandler();
			break;
		}
		return rsp;
	}

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
	public boolean onCreate() {
		init();
		return true;
	}

	private void init()
	{
		mOpenHelper = new DatabaseHelper(getContext(), DATABASE_NAME, null);
		writableDatabase = mOpenHelper.getWritableDatabase();
		readableDatabase = mOpenHelper.getReadableDatabase();
		Runnable itemsUpdater = new Runnable() {
            @Override
            public void run() {
                updateItemsList();   
            }
        };
        new Thread(itemsUpdater).start();
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, 
						String[] selectionArgs, String sortOrder) {
		Cursor queryCursor = null;
		int match = sUriMatcher.match(uri);
		ContentType type = ContentType.UNKNOWN;

		final NormalizedUri normalizedUri = NormalizedUri.fromAndroidUri(uri);

		switch (match) {
		case VIDEO_ID:
			type = ContentType.VIDEO;
		case AUDIO_ID:
			if (type == ContentType.UNKNOWN)
				type = ContentType.AUDIO;
		case INTEGRAL_POST:
		{
			if (type == ContentType.UNKNOWN)
				type = ContentType.TEXT;
			String uri_path = normalizedUri.getPath();
			String select = ContentsTable.COLUMN_URI + " = '" + uri_path + "'";
			
			queryCursor = getReadableDatabase().query(ContentsTable.TABLE_NAME, 
					  projection, 
					  select, 
					  selectionArgs, 
					  null, 
					  null, 
					  sortOrder);
			queryCursor.setNotificationUri(getContext().getContentResolver(), uri);
			
			if (!"".equals(uri_path)) {
				Log.i(tag, "Sending request for name " + uri_path + " uri path" + uri_path);
				asynchQueryRequest(uri_path, normalizedUri);
			}
			
			break;
		}
	    case ARCHIVE:
	       queryCursor = writableDatabase.query(ContentsTable.TABLE_NAME, 
                     projection, 
                     selection, 
                     selectionArgs, 
                     null, 
                     null, 
                     sortOrder);
           queryCursor.setNotificationUri(getContext().getContentResolver(), uri);
           
           break;
           
		default:
			Log.w(tag, "Cannot match uri " + uri);
		}
		
		return queryCursor;
	}

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        SQLiteDatabase db = getWritableDatabase();
        if (db.insert(ContentsTable.TABLE_NAME, null, values) == -1) {
            Log.e(tag, "Error inserting into database");
        }
        
        Uri newUri = Uri.withAppendedPath(ContentsTable.CONTENTS_URI, uri.getEncodedPath());
        getContext().getContentResolver().notifyChange(newUri, null);
        
        return newUri;
    }

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = 0;
        switch (sUriMatcher.match(uri)) {
            case INTEGRAL_POST:
                Log.i(tag, "Updating database: " + uri + " selection = " + selection);
                count = db.update(ContentsTable.TABLE_NAME, values, selection, selectionArgs);
                break;

            default:
                Log.e(tag, "Unsupported URI");
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
	}

	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode) 
			throws FileNotFoundException {
		//supporting read only files for now
		if (!"r".equals(mode.toLowerCase())) {
			throw new FileNotFoundException("Unsupported mode, " +
					mode + ", for uri: " + uri);
		}	
		return openFileHelper(uri, mode);
	}
}
