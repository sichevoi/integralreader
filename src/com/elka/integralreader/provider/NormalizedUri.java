package com.elka.integralreader.provider;


import java.net.URI;
import java.util.List;

import android.net.Uri;

public final class NormalizedUri {
    private final static String tag = NormalizedUri.class.getSimpleName();
    private final String path;
    
    public NormalizedUri(String path) {
        this(Uri.parse(path).getPathSegments());
    }
    
    public NormalizedUri(List<String> parts) {
        StringBuilder pathBuilder = new StringBuilder();
        for ( String part : parts ) {
            pathBuilder.append("/");
            pathBuilder.append(part);
        }
        path = pathBuilder.toString();
    }
    
    public Uri getLocalUri() {
        return Uri.withAppendedPath(ContentsTable.CONTENTS_URI, this.path);
    }
    
    public Uri getQueryUri() {
        return Uri.withAppendedPath(ReaderContentProvider.QUERY_URI, this.path);
    }
    
    public String getPath() {
        return path;
    }
    
    public static NormalizedUri fromAndroidUri(Uri androidUri) {
        return new NormalizedUri(androidUri.getPathSegments());
    }
    
    public static NormalizedUri fromJavaNetURI(URI javaUri) {
        return fromAndroidUri(Uri.parse(javaUri.toString()));
    }
}
