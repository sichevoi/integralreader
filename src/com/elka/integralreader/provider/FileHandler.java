package com.elka.integralreader.provider;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;

public class FileHandler implements ResponseHandler<String> {
	private String mCacheDir;
	private String mId;
	
	public FileHandler(String cacheDir, String id) {
		mCacheDir = cacheDir;
		mId = id;
	}
	
	public String getFileName() {
		return mCacheDir + "/" + mId;
	}
	@Override
	public String handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		InputStream urlStream = response.getEntity().getContent();
		FileOutputStream fout = new FileOutputStream(getFileName());
		
		byte[] bytes = new byte[256];
		int r = 0;
		do {
			r = urlStream.read();
			if (r > 0) {
				fout.write(bytes);
			}
		} while (r > 0);
			
		urlStream.close();
		fout.close();
		
		return null;
	}
}
