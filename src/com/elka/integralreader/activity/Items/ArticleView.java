package com.elka.integralreader.activity.Items;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.database.Cursor;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.elka.integralreader.provider.ContentsTable;
import com.elka.integralreader.provider.NormalizedUri;
import com.elka.integralreader.utils.FileManager;
import com.elka.integralreader.R;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 07.03.12
 * Time: 23:49
 * To change this template use File | Settings | File Templates.
 */
public class ArticleView extends SherlockFragment 
				implements LoaderManager.LoaderCallbacks<Cursor> {

    private WebView webView;
    private final NormalizedUri uri;
    
    private static final String mLogSs = "ARTICLE_VIEW";
    private static final String htmlHeader = "<html><head></head><body style=\"text-align:justify;color:black;background-color:#faeedd;\">";
    private static final String htmlFooter = "</body></html>";

    public ArticleView(String path)
    {
    	this.uri = new NormalizedUri(path);
    	setHasOptionsMenu(true);
    	setHasOptionsMenu(true);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	inflater.inflate(R.menu.content_menu, menu);

        /* set up a listener for the refresh item
        final MenuItem refresh = (MenuItem) menu.findItem(R.id.menu_refresh);
        refresh.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            // on selecting show progress spinner for 1s
            public boolean onMenuItemClick(MenuItem item) {
                // item.setActionView(R.layout.progress_action);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refresh.setActionView(null);
                    }
                }, 1000);
                return false;
            }
        });*/
        super.onCreateOptionsMenu(menu, inflater);
        
        return;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.articleview, container, false);
    }    
    
    @Override 
    public void onStart()
    {
    	super.onStart();
        webView = (WebView) getView().findViewById(R.id.articleview);
        
        //TODO: move this into the xml
        webView.setBackgroundColor(Color.argb(255, 250, 238, 221));
    }
    
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{
		// Create and return a CursorLoader that will take care of
		Log.i(mLogSs, "Attempting to load NormalizedUri " + uri.getPath());
		return new CursorLoader(getActivity(), uri.getLocalUri(),
                null, null, null,null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		Log.i(mLogSs, "onLoadFinished");
		if (cursor.moveToFirst()) {
		    String fileName = cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_FILE));
		    String data = (String) FileManager.get(fileName);
		    if(data != null) {
		        Log.i(mLogSs, "onLoadFinished - loading the view");
		        StringBuilder content = new StringBuilder();
		        content.append(htmlFooter);
		        content.append(data);
		        content.append(htmlHeader);
		        webView.loadDataWithBaseURL(null, content.toString(),"text/html", null, null);
		    } else {
		        Log.i(mLogSs, "onLoadFinished - no file cached");
		    }
		} else {
		    Log.i(mLogSs, "onLoadFinished - cursor is zero");
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
}
