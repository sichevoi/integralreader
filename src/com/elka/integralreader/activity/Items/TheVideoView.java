package com.elka.integralreader.activity.Items;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Vector;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.R;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.utils.PageParser;

/**
 * User: homyak
 * Date: 22.04.12
 * Time: 20:43
 * To change this template use File | Settings | File Templates.
 */
@TargetApi(3)
public class TheVideoView extends SherlockFragment implements DownloadSubscriber
{
    private static final String mLogSs = "VIDEO_VIEW";
    private String mURi;		//path to the page URL, which contains the video player.
    WebView mWebView;
    String mPlaylistPath;		//playlist path - to be extracted from the page
    							//and passed into the player
    PageParser mPageParser;
    ProgressBar mProgress;
    
    String htmlPre = "<html lang=\"en\"><head><meta charset=\"utf-8\"></head><body style='margin:0; pading:0; background-color: black;'>";
    String htmlCode =
            "<embed width=\"100%\" height=\"100%\" name=\"plugin\" src=" + 
            "\"http://integrallife.com/sites/all/modules/il_jwplayer/mediaplayer-5.9/player.swf" +
            "?playlistfile=@PLAYLIST@" +
            "&controlbar=over\"" +
            " type=\"application/x-shockwave-flash\" " +
            "";
    String htmlPost = "</body></html>";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * TODO: extract fragment's extras here
         */
        
        /*
        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            mUri = extras.getString("ACTION_URI");
            Log.i(mLogSs, mUri);
        }
        */

        int[] arr = new int[1];
        arr[0] = PageParser.SCRIPT;
        mPageParser = new PageParser(arr);
        
        new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_ARTICLE).execute(mURi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.videoplay, container, false);
    }
    
    @Override
    public void onStart()
    {
        mWebView = (WebView)getView().findViewById(R.id.videoview);
        //mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setPluginsEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY); //thanks Patrick!
    }
    
    private void callHiddenWebViewMethod(String name){
    	// credits: http://stackoverflow.com/questions/3431351/how-do-i-pause-flash-content-in-an-android-webview-when-my-activity-isnt-visible
        if( mWebView != null ){
            try {
                Method method = WebView.class.getMethod(name);
                method.invoke(mWebView);
            } catch (NoSuchMethodException e) {
                Log.e(mLogSs, "No such method: " + name + e);
            } catch (IllegalAccessException e) {
                Log.e(mLogSs, "Illegal Access: " + name + e);
            } catch (InvocationTargetException e) {
                Log.e(mLogSs, "Invocation Target Exception: " + name + e);
            }
        }
    }

    /**
     * Process - delivers content read from the input stream to the subscriber
     *
     * @param s - a piece of content
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(String s) {
    	// we are looking for the playlist only
    	// that should be defined in the player script:
    	// <script type="text/javascript">
    	// <!--//--><![CDATA[//><!--
    	// 
    	// $(function() { // wait for the DOM to load using jQuery
    	// 
    	//	var jw = jwplayer("jwplayer_container").setup({
    	//        playlistfile: "/il_playlist/1564/playlist.xml",
    	//        height: 351,
    	//        width: 700,
    	//        "controlbar.idlehide": true,
    	//        controlbar: "over",
    	//        repeat: "list",
    	//    	skin: "/sites/all/modules/il_jwplayer/lightrv5.zip",
    	//        "playlist.position": "left",
    	//        "playlist.size": 232,
    	//		modes: [
    	//			{type: 'flash', src: '/sites/all/modules/il_jwplayer/mediaplayer-5.9/player.swf'},
    	//			//{type: 'html5'},
    	//			//{type: 'download'}
    	//		]
    	//    });
    	//
    	// });
    	
    	if (mPlaylistPath == null)    //check if the playlist already found
    	{
    		Vector<String> ret = mPageParser.add(s);
    		for (int i = 0; i < ret.size(); ++i)
    		{
    			if (ret.elementAt(i).contains("jwplayer_container"))
    			{
    				Log.i(mLogSs, "Found jwplayer_container script");
    				String[] s0 = ret.elementAt(i).split("playlistfile:");
    				if (s0.length > 1)
    				{
    					String[] s1 = s0[1].split(",");
    					mPlaylistPath = "http://integrallife.com/" + s1[0].replace("\"", "").replace(" ", "");
    					Log.i(mLogSs, "Got the playlist path " + mPlaylistPath);
    					break;
    				}
    			}
    		}
    	}
    	
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onPause(){
        super.onPause();

        callHiddenWebViewMethod("onPause");
/*
        mWebView.pauseTimers();
        if(isFinishing()){
        	mWebView.loadUrl("about:blank");
            setContentView(new FrameLayout(this));
        }
*/
    }

    @Override
    public void onResume(){
        super.onResume();

        callHiddenWebViewMethod("onResume");

        mWebView.resumeTimers();
    }

	@Override
	public void UpdateFinal() {
        htmlCode = htmlCode.replaceAll("@PLAYLIST@", mPlaylistPath);
        Log.i(mLogSs, htmlPre+htmlCode+htmlPost);
        mWebView.loadDataWithBaseURL("fake://fake/fake", htmlPre+htmlCode+htmlPost, "text/html", "UTF-8", null);	
	}
	
	
	// not used
    /**
     * Process - delivers content read from the input stream to the subscriber.
     * This one can be called from the UI thread
     *
     * @param img - a drawable object to process
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(Drawable img, String path) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Finalize - to be called after the content is 0
     * Not in the UI thread
     */
    public void Finalize() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
    
    public void Update(CharSequence update) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
