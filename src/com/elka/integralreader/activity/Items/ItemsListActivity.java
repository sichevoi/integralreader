package com.elka.integralreader.activity.Items;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.elka.integralreader.provider.NormalizedUri;
import com.elka.integralreader.utils.ILRTypes.MediaType;
import com.elka.integralreader.utils.TransactionListener;
import com.elka.integralreader.R;

public class ItemsListActivity extends SherlockFragmentActivity 
			implements TransactionListener {

    ProgressDialog mDialog;
    //private String[] ItemsList;
    //private String uri;
    private String mLogSs = "ItemsListActivity";
    private NormalizedUri mUri;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items_list_activity);
        
        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
        	mUri = new NormalizedUri(extras.getString("ACTION_URI"));
        }
  
    }
    
    @Override
    public void onStart()
    {
        super.onStart();
        
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        
        ItemsListView frag = new ItemsListView(mUri, (TransactionListener)this);
        fragmentTransaction.add(R.id.items_list_activity, frag);
        fragmentTransaction.commit();       	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main_menu, menu);

        /* set up a listener for the refresh item
        final MenuItem refresh = (MenuItem) menu.findItem(R.id.menu_refresh);
        refresh.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            // on selecting show progress spinner for 1s
            public boolean onMenuItemClick(MenuItem item) {
                // item.setActionView(R.layout.progress_action);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refresh.setActionView(null);
                    }
                }, 1000);
                return false;
            }
        });*/
        return super.onCreateOptionsMenu(menu);
    }
	@Override
	public void onStartTransaction(MediaType type, String path) {
		// TODO Auto-generated method stub
        switch (type)
        {
            case ARTICLE:
            {
            	Log.w(mLogSs, "ITEM_ARTICLE Clicked");
            	/*
                Intent i = new Intent(ItemsListView.this, ArticleView.class);
                i.putExtra("ACTION_URI", uri);
                startActivityForResult(i, WEB_VIEW);
                */
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                
                ArticleView frag = new ArticleView(path);
                fragmentTransaction.replace(R.id.items_list_activity, frag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
            break;

            case VIDEO:
            {
            	Log.w(mLogSs, "ITEM_VIDEO Clicked");
            	/*
                Intent i = new Intent(ItemsListView.this, VideoViewer.class);
                i.putExtra("ACTION_URI", uri);
                startActivityForResult(i, WEB_VIEW);
                */
            }
            break;

            case ART_GALLERY:
            {
            	Log.w(mLogSs, "ITEM_ART_GALLERY Clicked");
            	/*
                Intent i = new Intent(ItemsListView.this, GalleryThumbnails.class);
                i.putExtra("ACTION_URI", uri);
                startActivityForResult(i, WEB_VIEW);
                */
            	
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                
                GalleryThumbnails frag = new GalleryThumbnails(path);
                fragmentTransaction.replace(R.id.items_list_activity, frag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
            break;

            case AUDIO:
            {
            	Log.w(mLogSs, "ITEM_ART_GALLERY Clicked");
            }
            break;
            
            default:
            {
                Log.e(mLogSs, "Unknown content type!");
            	/*
                Intent i = new Intent(ItemsListView.this, ArticleView.class);
                i.putExtra("ACTION_URI", uri);
                startActivityForResult(i, WEB_VIEW);
                */
            }
        }
	}
    
}
