/**
 * 
 */
package com.elka.integralreader.activity.Items;

import java.util.Vector;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.provider.NormalizedUri;
import com.elka.integralreader.utils.CustomResource.Type;
import com.elka.integralreader.utils.MyUtils;
import com.elka.integralreader.utils.PageParser;
import com.elka.integralreader.utils.ResourceStorage;
import com.elka.integralreader.R;

/**
 * Intended to represent the images thumbnails in a grid view. 
 * Ideally would then transform into the ImageGallery with the 
 * starting point at the selected image.
 * The thumbnails have to be cached.
 * 
 * @author homyak
 *
 */
public class GalleryThumbnails extends SherlockFragment implements DownloadSubscriber {
	
    PageParser mParser;
    String mLogSs = "GALLERY_THUMBS";
    String mLink;
    String mPath;
    NormalizedUri mUri;
    
    GridView mGridView;
    int mNumImages;

    ResourceStorage mResources;
    
    private static final int STATE_INIT = 0;
    private static final int STATE_PROCESSING = 1;
    private static final int STATE_TRANSITIONING = 2;
    private int mState;
	
    StringBuilder mThumbnails;
    String htmlHead = "<html><head></head><body>";
    String htmlFoot = "</body></html>";
    
    DownloadContentTask mGalleryDownloader;
    
    public GalleryThumbnails(String path)
    {
    	mUri = new NormalizedUri(path);
    }
    
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try
        {
        	mNumImages = 0;
        	mThumbnails = new StringBuilder();
        	mThumbnails.append(htmlHead);
        	mResources = ResourceStorage.getInstance();
            mState = STATE_INIT;
            int[] arr = new int[1];
            arr[0] = PageParser.IFRAME;
            mParser = new PageParser(arr);
            // We first download the page itself
            // TODO: enable NormalizedUri loading here
            mGalleryDownloader = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_RAW);
            //mGalleryDownloader.execute(mUri);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        finally
        {
            //urlConnection.disconnect();
        }
        
        ResourceStorage.getInstance().clearResourceSet(Type.TYPE_GALLERY);
        
        /*
        mGridView = (GridView) findViewById(R.id.thumbsview);
        mGridView.setAdapter(new GalleryAdapter(this));
        */
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.gallerythumbs, container, false);
    }
    
    @Override
    public void onStart()
    {
    	super.onStart();
        mGridView = (GridView) getView().findViewById(R.id.thumbsview);
        mGridView.setAdapter(new GalleryAdapter(this.getActivity().getApplicationContext()));
    }

    @Override
    public void onPause()
    {
    	super.onPause();
    	
    	if (mGridView.getAdapter().getClass() == GalleryAdapter.class)
    	{
    		GalleryAdapter a = (GalleryAdapter)mGridView.getAdapter();
    		a.notifyDataSetInvalidated();
    	}
    }
    
	/* (non-Javadoc)
	 * @see com.example.IDownloadSubscriber#Update(java.lang.CharSequence)
	 */
	@Override
	public void Update(CharSequence update) {
		Log.i(mLogSs, "Got the update!!!");
		if (mGridView.getAdapter().getClass() == GalleryAdapter.class)
		{
			GalleryAdapter a = (GalleryAdapter)mGridView.getAdapter();
			a.Update();
		}
		else
		{
			Log.e(mLogSs, "The adapter is not of our type!!!");
		}
	}

	/* We received some data - need to process it
	 * @see com.example.IDownloadSubscriber#Process(java.lang.String)
	 */
	@Override
	public boolean Process(String s) {
		Vector<String> ret;
		ret = mParser.add(s);
        if (mState == STATE_INIT)
        {
            // TODO - handle
            if (ret.size() > 0)
            {
            	String iframe = ret.elementAt(0);
                Log.i(mLogSs, "iframe found: " + iframe);
                /**
                 * Check if that is a correct id
                 */
                if (iframe.contains("id=\"external-url-frame"))
                {
                    Log.i(mLogSs, "Iframe id is gallery-frame, extracting the link");
                    String s0[] = iframe.split("http://");
                    if (s0.length > 1)
                    {
                        String s1[] = s0[1].split("\"");
                        String link = "http://";
                        link = link + s1[0];
                        Log.i(mLogSs, "Link to the gallery: " + link);
                   	
                        mPath = link;
						mLink = MyUtils.getPath(link);
						Log.i(mLogSs, "The base URL " + mLink);
						
                        mState = STATE_TRANSITIONING;
                    }
                }
            }
        }
        else if (mState == STATE_PROCESSING)
        {
        	//Log.i(mLogSs, "Receiving the images " + s);
        	Log.i(mLogSs, "Found " + ret.size() + " tags");
        	if (ret.size() > 0)
        	{
        		for(int i = 0; i < ret.size(); ++i)
        		{
        			String td = ret.elementAt(i);
        			if (td.contains("class='lightview"))
        			{
        				//Log.i(mLogSs, "Found the image td: " + td);
        				String s1[];
        				
        				String s0[] = td.split("<a href='");
        				if (s0.length > 1)
        				{
        					s1 = s0[1].split("'");
        					//Log.i(mLogSs, "The image path is " + mLink + s1[0]);
        					++mNumImages;
        				}
        				
        				s0 = td.split("<img src=\"");
        				if (s0.length > 1)
        				{
        					s1 = s0[1].split("\"");
        					//Log.i(mLogSs, "The image thumb path is " + mLink + s1[0]);
        					mThumbnails.append("<img src=\"" + mLink + s1[0] + "\" />");
        				}
        			}
        		}
        	}
        	
        }
		return false;
	}

	/* (non-Javadoc)
	 * @see com.example.IDownloadSubscriber#Process(android.graphics.drawable.Drawable)
	 */
	@Override
	public boolean Process(Drawable img, String path) {
		GalleryImage gi = new GalleryImage(path);
		gi.setImageThumb(img);
		//mResources.addToResourceSet((CustomResource)gi);
		return false;
	}

	/* (non-Javadoc)
	 * @see com.example.IDownloadSubscriber#Finalize()
	 */
	@Override
	public void Finalize() {
		Log.i(mLogSs, "total of " + mNumImages + " images found");
		mThumbnails.append(htmlFoot);
		//Log.i(mLogSs, mThumbnails.toString());
		Html.fromHtml(mThumbnails.toString(), mGalleryDownloader.IlImageGalleryGetterGet(),null);
		//mGalleryDownloader.requestUpdate();
		//mGalleryDownloader.execute("");
	}


	@Override
	public void UpdateFinal() {
		if (mState == STATE_TRANSITIONING)
		{
			mState = STATE_PROCESSING;
            int[] arr = new int[1];
            arr[0] = PageParser.TABLES_TD;
            mParser = new PageParser(arr);
            mGalleryDownloader = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_GALLERY);
            mGalleryDownloader.execute(mPath);
		}
		
	}

}
