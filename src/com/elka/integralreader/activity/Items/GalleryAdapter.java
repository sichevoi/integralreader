package com.elka.integralreader.activity.Items;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.elka.integralreader.utils.CustomResource.Type;
import com.elka.integralreader.utils.CustomResource;
import com.elka.integralreader.utils.ResourceStorage;

public class GalleryAdapter extends BaseAdapter
{
    //int mGalleryItemBackground;
    private Context mContext;
    private int mNumListItems;
    ResourceStorage mRes;
    private Vector<CustomResource> mImageList;

    private String mLogSs = "GALLERY_ADAPTER";
    
    public GalleryAdapter(Context c) {
        mContext = c;
        //TypedArray a = obtainStyledAttributes(android.R.styleable.Theme);
        //mGalleryItemBackground = a.getResourceId(
                //android.R.styleable.Theme_galleryItemBackground, 0);
        //a.recycle();
        mRes = ResourceStorage.getInstance();
        mImageList = mRes.getResourceSet(Type.TYPE_GALLERY);
        mNumListItems = mImageList.size();
    }

    public int getCount() {
        return mNumListItems;
    }

    public Object getItem(int position) {
    	if (position < mImageList.size())
    	{
    		if (mImageList.elementAt(position).getClass() == GalleryImage.class)
    		{
    			GalleryImage img = (GalleryImage)mImageList.elementAt(position);
    			return img.getImageThumb();
    		}
    	}
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = new ImageView(mContext);
            ((ImageView)convertView).setAdjustViewBounds(true);
        }

		if (mImageList.elementAt(position).getClass() == GalleryImage.class)
		{
			GalleryImage img = (GalleryImage)mImageList.elementAt(position);
	        ((ImageView)convertView).setImageDrawable(img.getImageThumb());
	        //((ImageView)convertView).setLayoutParams(new Gallery.LayoutParams(150, 100));
	        ((ImageView)convertView).setScaleType(ImageView.ScaleType.CENTER_INSIDE);
	        ((ImageView)convertView).setPadding(20,20,20,20);
		}

        return convertView;
    }
    
    public void Update()
    {
    	mImageList = mRes.getResourceSet(Type.TYPE_GALLERY);
        mNumListItems = mImageList.size();
        notifyDataSetChanged();
    }
}
