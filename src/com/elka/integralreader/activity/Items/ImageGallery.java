package com.elka.integralreader.activity.Items;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import java.util.Vector;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.R;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.utils.PageParser;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 07.05.12
 * Time: 21:25
 * To change this template use File | Settings | File Templates.
 */
public class ImageGallery extends SherlockFragment implements DownloadSubscriber
{
    PageParser mParser;
    String mLogSs = "IMAGE_GALLERY";
    Gallery mGalleryView;

    private static final int STATE_INIT = 0;
    private static final int STATE_PROCESSING = 1;
    private int mState;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String URi = null;

        /*
        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            URi = extras.getString("ACTION_URI");
            Log.i("ARTICLE_VIEW", URi);
        }
		*/
        try
        {
            mState = STATE_INIT;
            int[] arr = new int[1];
            arr[0] = PageParser.IFRAME;
            mParser = new PageParser(arr);
            new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_RAW).execute(URi);
        }
        catch (Exception e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        finally
        {
            //urlConnection.disconnect();
        }

        /*
        mGalleryView = (Gallery) findViewById(R.id.gallery);
        mGalleryView.setAdapter(new GalleryAdapter(this));

        mGalleryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

            }
        });
		*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.imagegallery, container, false);
    }
    
    public void onStart()
    {
    	super.onStart();
        mGalleryView = (Gallery) getView().findViewById(R.id.gallery);
        mGalleryView.setAdapter(new GalleryAdapter(getActivity()));

        mGalleryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressWarnings("rawtypes")
			public void onItemClick(AdapterView parent, View v, int position, long id) {

            }
        });    	
    }
    
    public void Update(CharSequence update) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void Update(Drawable img) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Process - delivers content read from the input stream to the subscriber
     *
     * @param s - a piece of content
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(String s)
    {
        if (mState == STATE_INIT)
        {
            Vector<String> ret = mParser.add(s);
            // TODO - handle
            
            if (ret.size() > 0)
            {
            	String iframe = ret.elementAt(0);
                Log.i(mLogSs, "iframe found: " + iframe);
                /**
                 * Check if that is a correct id
                 */
                if (iframe.contains("id=\"external-url-frame"))
                {
                    Log.i(mLogSs, "Iframe id is gallery-frame, extracting the link");
                    String s0[] = iframe.split("http://");
                    if (s0.length > 1)
                    {
                        String s1[] = s0[1].split("\"");
                        String link = "http://";
                        link = link + s1[0];
                        Log.i(mLogSs, "Link to the gallery: " + link);
                        mState = STATE_PROCESSING;
                        ProcessGallery(link);
                    }
                }
            }
        }
        else if (mState == STATE_PROCESSING)
        {

        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Process - delivers content read from the input stream to the subscriber.
     * This one can be called from the UI thread
     *
     * @param img - a piece of content
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(Drawable img, String path) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private void ProcessGallery(String s)
    {
        try {
            int[] arr = new int[1];
            arr[0] = PageParser.MAIN_TABLE;
            mParser = new PageParser(arr);
            new DownloadContentTask((DownloadSubscriber)mGalleryView.getAdapter(), DownloadContentTask.TYPE_GALLERY).execute(s);
        }
        catch (Exception e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    /**
     * Finalize - to be called after the content is 0
     */
    public void Finalize() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

	@Override
	public void UpdateFinal() {
		// TODO Auto-generated method stub
		
	}

}


