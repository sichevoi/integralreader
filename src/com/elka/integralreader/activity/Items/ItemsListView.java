package com.elka.integralreader.activity.Items;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.elka.integralreader.download.images.ImageLoadedListener;
import com.elka.integralreader.download.images.ImageProvider;
import com.elka.integralreader.provider.ContentsTable;
import com.elka.integralreader.provider.NormalizedUri;
import com.elka.integralreader.utils.ILRTypes.MediaType;
import com.elka.integralreader.utils.*;
import com.elka.integralreader.R;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 07.03.12
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class ItemsListView extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    private ListView mLv;
    private ProgressDialog mDialog;
    private ContentListAdapter mListAdapter;
    private final NormalizedUri mUri;
    private final TransactionListener mListener;
    private final String mLogSs = "ItemsListView";
    private Boolean mSelecting;
    private Handler handler;
    private static final int IMAGE_CACHE_SIZE = 50;

    
    private static final class ViewHolder {
        NetworkImageView thumbnail;
        TextView title;
        TextView contributors;
    }
    
    public ItemsListView(NormalizedUri uri, TransactionListener listener)
    {
    	mUri = uri;
    	mListener = listener;
    	mListAdapter = null;
    	mSelecting = false;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
        handler = new Handler();
    }
    
    @Override 
    public void onStart()
    {
    	super.onStart();
        
        mLv = getListView();
        TextView footerView = new TextView(this.getActivity().getApplicationContext());
        footerView.setText("More...");
        mLv.addFooterView(footerView, "Footer view", true);
        
        /** Setting the list adapter for the ListFragment */
    	if (null == mListAdapter) {
    		mListAdapter = new ContentListAdapter(getActivity().getLayoutInflater(), 
    		                        handler,
    		                        ImageProvider.buildLoader(getActivity(), IMAGE_CACHE_SIZE));
    		setListAdapter(mListAdapter);
    		setListShown(false);
    	}
    	
    	ContentViewConfig();
    	
    	//getActivity().registerForContextMenu(mLv);
    	mLv.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v,
                            ContextMenuInfo menuInfo) {
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.items_list_context_menu, menu);
                
                Log.i(mLogSs, "Creating the context menu");
            }}); 
    }

    private void ContentViewConfig()
    {
        mLv.setBackgroundColor(Color.argb(255, 255, 255, 255));
        mLv.setCacheColorHint(Color.argb(255, 255, 255, 255));
        mLv.setTextFilterEnabled(true);
        
        mLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	// need to handle the footer view click here
        	if (parent.getAdapter().getItem(position).getClass() == ListItem.class) {
        		Log.v("onItemClick", "ListItems clicked");
	
        		if (null != mListener) {
        			ListItem item = (ListItem)parent.getAdapter().getItem(position);
        			if (!mSelecting) {
        				mListener.onStartTransaction(item.getType(), item.getPath());
        			} else {
            			if (view.getClass() == LinearLayout.class) {
            				if (position - mLv.getFirstVisiblePosition() > 0) {
            					View v = mLv.getChildAt(position - mLv.getFirstVisiblePosition());
    	        				if (v != null) {
    	        					CheckedTextView ct = (CheckedTextView) v.findViewById(R.id.itemTitle);
    	        					if (item.getChecked()) {
    	        						ct.setChecked(false);
    	        						item.setChecked(false);
    	        					} else {
    	        						ct.setChecked(true);
    	        						item.setChecked(true);
    	        					}
    	        				} else {
    	        					Log.e(mLogSs, "PANEKO PANEKO child is null!!!!");
    	        				}
            				} else {
            					Log.e(mLogSs, "PANEKO first visible position is more that position!");
            				}
            			}
        				Log.i(mLogSs, "In multiple selectin mode");
        			}
        		}
        	} else {
        		Log.i("onItemClick", "Footer clicked " + parent.getAdapter().getClass().getName());
        		if(parent.getAdapter().getClass() == HeaderViewListAdapter.class) {
        			Log.i("onItemClick", "Footer clicked");
        		}
        	}
        }
        });
        
        mLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Log.i("onItemLongClick", " fired");
				mSelecting = true;
				// TODO Auto-generated method stub
				return true;
			}
        	
		});    
        //mLv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        //mLv.setItemChecked(position, value)
    }
    
    private static final class ListItem {
        Boolean mChecked;
        final String title;
        final String path;
        final MediaType type;
        final String contributors;
        final NormalizedUri imagePath;

        public ListItem(String title, String path, MediaType type, String imagePath, String contributors) {
            mChecked = false;
            this.title = title;
            this.path = path;
            this.type = type;
            this.contributors = contributors;
            this.imagePath = new NormalizedUri(imagePath);
        }
        
        public String getImageUrl() {
            return imagePath.getQueryUri().toString();
        }
        

        public String getPath() {
            return path;
        }

        public MediaType getType() {
            return type;
        }
        
        
        public Boolean getChecked() {
            return mChecked;
        }
        
        public void setChecked(Boolean s) {
            mChecked = s;
        }
    }
    
    private static final class ContentListAdapter extends BaseAdapter
    {
        private static final String tag = ContentListAdapter.class.getSimpleName();
        private final LayoutInflater inflator;
        private final Handler handler;
        private final ImageLoader imageLoader;


        private final List<ListItem> itemsList = new ArrayList<ListItem>();

        ContentListAdapter(LayoutInflater inflator, Handler handler, ImageLoader imageLoader)
        {
            this.inflator = inflator;
            this.handler = handler;
            this.imageLoader = imageLoader;
        }
        
        public void Update(Cursor cursor) {
            int rowsCount = cursor.getCount();
            final List<ListItem> addedItems = new ArrayList<ListItem>(rowsCount);

            Log.i(tag, "Got Update, num rows = " + rowsCount);
            if (cursor.moveToFirst()) {
                do {
                    Log.i(tag, "Title = " + cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_TITLE)));
                    addedItems.add(new ListItem(
                            cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_URI)),
                            MediaType.fromInt(cursor.getInt(cursor.getColumnIndex(ContentsTable.COLUMN_TYPE))),
                            cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_THUMB)),
                            cursor.getString(cursor.getColumnIndex(ContentsTable.COLUMN_CONTRIB))
                            ));
                } while (cursor.moveToNext());
            }
            
            handler.post(new Runnable() {
                @Override
                public void run() {
                    itemsList.addAll(addedItems);
                    notifyDataSetChanged();
                }
            });
            
        }
        
        public int getCount() {
            return itemsList.size();  //To change body of implemented methods use File | Settings | File Templates.
        }

        public Object getItem(int i) {
            return itemsList.get(i);  //To change body of implemented methods use File | Settings | File Templates.
        }

        public long getItemId(int i) {
            return 0;  //To change body of implemented methods use File | Settings | File Templates.
        }

        /**
         * TODO: use setCompoundDrawables instead of the image view
         */
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            if (null == view) {
                 view = inflator.inflate(R.layout.list_item, null);
                 ViewHolder holder = new ViewHolder();
                 holder.thumbnail = (NetworkImageView)view.findViewById(R.id.icon);
                 holder.title = (TextView)view.findViewById(R.id.itemTitle);
                 holder.contributors = (TextView)view.findViewById(R.id.contributors);
                 view.setTag(holder);
                 //convertView.setOnCreateContextMenuListener(null);
            }
            
            ViewHolder holder = (ViewHolder) view.getTag();
            holder.title.setText((CharSequence) itemsList.get(i).title);
            holder.thumbnail.setImageDrawable(null);
            holder.thumbnail.setImageUrl(null, imageLoader);
            holder.thumbnail.setImageUrl(itemsList.get(i).getImageUrl(), imageLoader);
            holder.contributors.setText(itemsList.get(i).contributors);
            
            return view;
        }
    }

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        Log.i(mLogSs, "Attempting to load NormalizedUri " + mUri.getPath());
        return new CursorLoader(getActivity(), mUri.getLocalUri(),
                null, null, null,null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
	    mListAdapter.Update(cursor);
	    handler.post(new Runnable() {
            @Override
            public void run() {
                setListShown(true);
            }
	    });	
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
    
    /*
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.items_list_context_menu, menu);
        
        Log.i(mLogSs, "Creating the context menu");
    }
    
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.list_menu_dnld:
                Log.i(mLogSs, "Selected the download menu");
                return true;
            case R.id.list_menu_cncl:
            	Log.i(mLogSs, "Selected the cancel menu");
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    */
}
