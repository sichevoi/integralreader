/**
 * 
 */
package com.elka.integralreader.activity.Items;

import java.io.Serializable;
import android.graphics.drawable.Drawable;
import com.elka.integralreader.utils.CustomResource;


/**
 * @author homyak
 *
 */
public class GalleryImage implements CustomResource, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3700520954162296357L;
	private String mImageName;
	private Drawable mImage;
	private Drawable mImageThumb;

	/**
	 * Contructor: accepts the name as a required parameter.
	 * There may not be an image without the name set.
	 * Name cannot be changed later, it's used as id.
	 * @param name
	 */
	public GalleryImage(String name)
	{
		mImageName = name;
		mImage = null;
		mImageThumb = null;
	}
	
	/* (non-Javadoc)
	 * @see com.example.ICustomResource#getId()
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return mImageName;
	}

	/* (non-Javadoc)
	 * @see com.example.ICustomResource#getType()
	 */
	@Override
	public Type getType() {
		// TODO Auto-generated method stub
		return CustomResource.Type.TYPE_GALLERY;
	}
	
	/**
	 * This is the actual _big_ image.
	 * May not nessesarily be set
	 * @param img
	 * @return
	 */
	public int setImage(Drawable img)
	{
		mImage = img;
		return 0;
	}
	
	/**
	 * This is the _small_ image thumbnail
	 * @param img
	 * @return
	 */
	public int setImageThumb(Drawable img)
	{
		mImageThumb = img;
		return 0;
	}
	
	public Drawable getImage()
	{
		return mImage;
	}
	
	public Drawable getImageThumb()
	{
		return mImageThumb;
	}
	
}
