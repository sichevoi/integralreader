package com.elka.integralreader.activity.IntegralLife;

import com.elka.integralreader.download.DownloadSubscriber;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * ArchiveItems handles the details for various archive sorting types.
 * Those are:
 * - channels
 * - topics
 * - contributors
 * - timeline
 * 
 * Each of those types has it's own list of entries, which can be obtained
 * using the appropriate URL. The URLs are statically stored in the 
 * ArchiveItems class.
 * 
 * @author homyak
 *
 */
public abstract class ArchiveItems implements DownloadSubscriber {

	int itemType;
	FragmentActivity mParent;
	LinearLayout 	mLinearLo; 
	ScrollView 		mScrollVw; 
	RelativeLayout 	mRelLo;
	
	private LayoutParams weightOne;
	private LayoutParams weightZero;
	protected String mLogSs = "ArchiveItems";
	ArchiveDropListItem[] mItems;

	/**
	 * URLs - a static array with URLS for each 
	 * list entry. Refer to Archive.java:
	 * 	CHANNELS = 0;
	 *  TOPICS = 1;
	 *  CONTRIBS = 2;
	 *  TIMELINE = 3;
	 * 
	 */
	private static final String[] URLs = {
		"http://integrallife.com/channels",
		"http://integrallife.com/tags",
		"http://integrallife.com/contributors",
		"http://integrallife.com/archive/by-month-all"
	};
	
	static public ArchiveItems getItem(int type, FragmentActivity parent)
	{
		ArchiveItems ai = null;
		
		switch (type)
		{
		case Archive.CHANNELS:
			ai = new ArchiveChannels(type, parent);
			break;
		case Archive.TOPICS:
			ai = new ArchiveTopics(type, parent);
			break;
		case Archive.CONTRIBS:
			ai = new ArchiveContribs(type, parent);
			break;
		case Archive.TIMELINE:
			ai = new ArchiveTimeline(type, parent);
			break;
		default:
			Log.e("ArchiveItems", "Incorrent type passes into getItem: " + type);
		}
		
		return ai;
	}
	
	public ArchiveItems(int type, FragmentActivity parent)
	{
		if (type < Archive.MAX)
			itemType = type;
		else
			itemType = 0;
		
		mParent = parent;
		
		weightOne = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT, 1.0f);

		weightZero = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
		
		/**
		 * Need to kick off download here.
		 * Page parser will be looking for something like this:
		 *   <div class="views-field-title">
         *      <span class="field-content"><a href="/contributors/alex-grey">Alex Grey</a></span>
  		 *	 </div>
		 */
		
        //dnldTask = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_ITEMS_LIST);
        //dnldTask.execute(URLs[itemType]);
	}
	
	public void initLayout(LinearLayout ll, ScrollView sv, RelativeLayout rl)
	{
		if(!ll.equals(mLinearLo))
		{
			mLinearLo = ll;
			mScrollVw = sv;
			mRelLo = rl;
		
			if (mItems == null)
			{
				//TODO: remove this
				TextView[] tx = new TextView[20];
				for (int i = 0; i < 20; i++) {
				    tx[i] = new TextView(mParent);
				    tx[i].setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
				    tx[i].setText("textviewNo" + itemType + i);
				    
				    ll.addView(tx[i]);
				}
			}
			else
			{
				for (int i = 0; i < mItems.length; ++i)
				{
					ll.addView(mItems[i]);
				}
			}
		}
	}
	
	/**
	 * Sets the items list to invisible:
	 * - reset the relative lo weight to 0
	 * - set scroll view visibility to GONE
	 */
	public void setInvisible()
	{
		mRelLo.setLayoutParams(weightZero);
		mScrollVw.setVisibility(View.GONE);
	}
	
	/**
	 * Sets the items list to invisible:
	 * - reset the relative lo weight to 1
	 * - set scroll view visibility to VISIBLE
	 */	
	public void setVisible()
	{
		mRelLo.setLayoutParams(weightOne);
		mScrollVw.setVisibility(View.VISIBLE);
	}
}
