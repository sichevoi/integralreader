package com.elka.integralreader.activity.IntegralLife;

import java.util.Vector;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.utils.PageParser;
import com.elka.integralreader.R;

/**
 * This activity is intended to provide access to the highlighted content.
 * That'll be those items represented on the IL NOW page within the following tags:
 * <div class="views_slideshow..."
 * <div class="views-row..."
 * @author homyak
 *
 */
public class LatestGreatest extends SherlockFragment implements DownloadSubscriber {

	int mNumItems;
	PageParser mParser;
	ProgressBar mProgress;
	ViewGroup mContainer; 
	
	/**
	 * TODO: this one is not the best practice. Eventually would prefer
	 * to go to some sort of state preserving instead.
	 */
	View mView;
	
	Boolean allInPlace;
	
	Vector<FeaturedItem> mFeatured;
	DownloadContentTask dnldTask;
	String mLogSs = "LATEST_GRATEST";

	public LatestGreatest()
	{
		allInPlace = false;
		mView = null;
		mContainer = null;
	}
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mNumItems = 0;
        mFeatured = new Vector<FeaturedItem>(10);
                
        /**
         * Prepare the page parser
         */
        int[] arr = new int[1];
        arr[0] = PageParser.VIEW_SLIDESHOW;
        mParser = new PageParser(arr);
        
        //mDialog = ProgressDialog.show(LatestGreatest.this, "", 
        //        "Loading. Please wait...", true);
        
        /**
         * Start the downloader
         */
        
        Log.i(mLogSs, "onCreate called - kicking off DownloadContentTask");
        dnldTask = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_GALLERY);
        dnldTask.execute("http://integrallife.com/now");
        
        
    }
    @Override
    public void onPause()
    {
    	super.onPause();
    	Log.i(mLogSs,  "onPause called");
    }

    @Override
    public void onResume()
    {
    	super.onResume();
    	Log.i(mLogSs,  "onResume called");
    }
    
    @Override
    public void onSaveInstanceState (Bundle outState)
    {
    	Log.i(mLogSs,  "onSaveInstanceState called");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	super.onCreateView(inflater, container, savedInstanceState);
    	if (mView == null)
    	{
    		Log.i(mLogSs, "onCreateView called - inflating");
    		mContainer = container;
    		mView = inflater.inflate(R.layout.latest, container, false);
    	}
    	else
    	{
    		((ViewGroup)mView.getParent()).removeView(mView);
   			Log.i(mLogSs, "Removed from container " + mContainer + " added to " + container);
   			if (mContainer != container)
   			{
   				allInPlace = false;
   				mContainer = container;
   			}
    	}

    	return mView;
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	/**
    	 * We can only start getting these views here, 
    	 * because in the onCreateView the view is not yet ready
    	 */
    	if (false == allInPlace)
    	{
    		Log.i(mLogSs, "Nothing is in place, creating new" + (ImageView) getView().findViewById(R.id.backgr1));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest1), 
	        		(ImageView) getView().findViewById(R.id.backgr1), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest2), 
	        		(ImageView) getView().findViewById(R.id.backgr2), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest3), 
	        		(ImageView) getView().findViewById(R.id.backgr3), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest4), 
	        		(ImageView) getView().findViewById(R.id.backgr4), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest5), 
	        		(ImageView) getView().findViewById(R.id.backgr5), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest6), 
	        		(ImageView) getView().findViewById(R.id.backgr6), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest7), 
	        		(ImageView) getView().findViewById(R.id.backgr7), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest8), 
	        		(ImageView) getView().findViewById(R.id.backgr8), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest9), 
	        		(ImageView) getView().findViewById(R.id.backgr9), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.latest10), 
	        		(ImageView) getView().findViewById(R.id.backgr10), mContainer));
	        allInPlace = true;
    	}
    	else
    	{
    		Log.i(mLogSs, "All seems in place, show");
    		for (int i = 0; i < mNumItems; ++i)
    		{
    			mFeatured.elementAt(i).show();
    		}
    	}

    }
    
	@Override
	public void Update(CharSequence update) {

	}

	@Override
	public void UpdateFinal() {
		/*
    	if (mDialog != null)
    	{
    		mDialog.cancel();
    		mDialog = null;
    	}
    	*/
		for (int i = 0; i < mNumItems; ++i)
		{
			mFeatured.elementAt(i).show();
		}
	}
	
	
	@Override
	public boolean Process(String s) {
		Vector<String> parsedTags = mParser.add(s);
		Log.v(mLogSs, "Found " + parsedTags.size() + " slideshow tags");
        int[] arr = new int[1];
        arr[0] = PageParser.VIEW_ROW;
        PageParser mUtilityParser = new PageParser(arr);
		for (int j = 0; j < parsedTags.size(); ++j)
		{			
	        Vector<String> rows = mUtilityParser.add(parsedTags.elementAt(j));

	        for(int i = 0; i < rows.size(); ++i)
	        {
	        	Log.v(mLogSs, "Adding a new raw element");
	        	mFeatured.elementAt(mNumItems).setRaw(rows.elementAt(i));
	        	++mNumItems;
	        }
	        
		}
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		return false;
	}

	@Override
	public void Finalize() {
		for (int i = 0; i < mNumItems; ++i)
		{
			mFeatured.elementAt(i).init();
		}
	}
}
