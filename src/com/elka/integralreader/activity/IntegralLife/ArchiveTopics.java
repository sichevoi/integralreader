package com.elka.integralreader.activity.IntegralLife;


import com.elka.integralreader.activity.Items.ItemsListActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

public class ArchiveTopics extends ArchiveItems {

	private static final String[] topics = {
		"Art & Creativity",
		"Biographies",
		"Education",
		"Entertainment",
		"Ethics",
		"Evolution",
		"Health & Wellness",
		"Integral Basics",
		"Integral Living",
		"Integral Theory",
		"Leadership",
		"Love & Intimacy",
		"Money",
		"Politics",
		"Practice Library",
		"Psychology",
		"Science & Technology",
		"Sex & Gender",
		"Spirituality",
		"Sustainability",
		"World Affairs",
		"All tags"
	};
	
	private static final String[] topicsUrls = {
		"http://integrallife.com/tags/art-creativity",  	//Art & Creativity",
		"http://integrallife.com/tags/biographies",			//Biographies",
		"http://integrallife.com/tags/education",			//Education",
		"http://integrallife.com/tags/entertainment",		//Entertainment",
		"http://integrallife.com/tags/ethics",				//Ethics",
		"http://integrallife.com/tags/evolution",			//Evolution",
		"http://integrallife.com/tags/health-wellness",		//Health & Wellness",
		"http://integrallife.com/tags/integral-basics",		//Integral Basics",
		"http://integrallife.com/tags/integral-living",		//Integral Living",
		"http://integrallife.com/tags/integral-theory",		//Integral Theory",
		"http://integrallife.com/tags/leadership",			//Leadership",
		"http://integrallife.com/tags/love-intimacy",		//Love & Intimacy",
		"http://integrallife.com/tags/money",				//Money",
		"http://integrallife.com/tags/politics",			//Politics",
		"http://integrallife.com/tags/practice-library",	//Practice Library",
		"http://integrallife.com/tags/psychology",			//Psychology",
		"http://integrallife.com/tags/science-technology", 	//Science & Technology",
		"http://integrallife.com/tags/sex-gender",			//Sex & Gender",
		"http://integrallife.com/tags/spirituality", 		//Spirituality",
		"http://integrallife.com/tags/sustainability", 		//Sustainability",
		"http://integrallife.com/tags/world-affairs", 		//World Affairs",
		"http://integrallife.com/tags", 					//All tags"		
	};

	private class ArchiveItemOnClick implements View.OnClickListener
	{
		@Override
		public void onClick(View v) {
			if (v.getClass() == ArchiveDropListItem.class)
			{
		        Intent i = new Intent(mParent, ItemsListActivity.class);
		        i.putExtra("ACTION_URI", ((ArchiveDropListItem)v).getUrl());
		        mParent.startActivityForResult(i, 0);
		    	Log.w(mLogSs, "onClickAllContent");
			}
		}
	}
	
	public ArchiveTopics(int type, FragmentActivity parent) {
		super(type, parent);
		
		// picked the size from the topics array length
		mItems = new ArchiveDropListItem[topics.length];
		
		for (int i = 0; i < mItems.length; ++i)
		{
			mItems[i] = new ArchiveDropListItem(parent, i);
			//mItems[i].setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			mItems[i].setUrl(topicsUrls[i]);
			mItems[i].setText(topics[i]);
			mItems[i].setOnClickListener(new ArchiveItemOnClick());
		}
	}

	@Override
	public void Update(CharSequence update) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean Process(String s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Finalize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void UpdateFinal() {
		// TODO Auto-generated method stub

	}

}
