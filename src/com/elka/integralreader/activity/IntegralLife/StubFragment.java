package com.elka.integralreader.activity.IntegralLife;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.R;

public class StubFragment extends SherlockFragment {
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	super.onCreateView(inflater, container, savedInstanceState);

    	return inflater.inflate(R.layout.stub_layout, container, false);
    }

}
