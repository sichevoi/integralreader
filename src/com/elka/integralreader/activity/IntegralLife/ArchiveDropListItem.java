package com.elka.integralreader.activity.IntegralLife;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class ArchiveDropListItem extends TextView {

	private String mUrl;

	public ArchiveDropListItem(Context context, int index) {
		super(context);
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		lp.setMargins(3, 1, 0, 1);
		setLayoutParams(lp);
		setTextSize (18.0f);
		setPadding (4, 4, 0, 4);
		if (index % 2 == 0)
			setBackgroundColor(Color.argb(70, 70, 68, 81));
		else
			setBackgroundColor(Color.argb(70, 69, 22, 28));
	}
	
	public void setUrl(String url)
	{
		mUrl = url;
	}
	
	public String getUrl()
	{
		return mUrl;
	}

}
