package com.elka.integralreader.activity.IntegralLife;

import java.util.Date;
import com.elka.integralreader.activity.Items.ItemsListActivity;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.utils.PageParser;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

/**
 * ArchiveTimeline class. Holds the drop down for the 
 * content structured by date. 
 * 
 * Downloads data from the following URL:
 * http://integrallife.com/archive/by-month-all
 * 
 * Here is the html structure:
 * <div id="content-area">
 * <div><a href="/archive/201211">NAME</a> (NUMBER_OF_ITEMS)</div>
 * </div>
 * 
 * Not all the month will be in the list - it'll be too long otherwise.
 * Keep 12 latest months - all previous years will be represented just by the year.
 * 
 * Need to extract the content-area first, then parse it for all other <div> entries.
 * TODO: Nice to do the above in one pass.
 * @author homyak
 *
 */
public class ArchiveTimeline extends ArchiveItems {

	PageParser mParser;
	PageParser mUtilityParser;
	
	DownloadContentTask dnldTask;
	
	static final int START_YEAR = 2003;
	static final int NUM_ITEMS = 30;
	
	private class ArchiveItemOnClick implements View.OnClickListener
	{
		@Override
		public void onClick(View v) {
			if (v.getClass() == ArchiveDropListItem.class)
			{
		        Intent i = new Intent(mParent, ItemsListActivity.class);
		        i.putExtra("ACTION_URI", ((ArchiveDropListItem)v).getUrl());
		        mParent.startActivityForResult(i, 0);
		    	Log.w(mLogSs, "ArchiveItemOnClick");
			}
		}
	}
	
	public ArchiveTimeline(int type, FragmentActivity parent) {
		super(type, parent);
		
		/**
		 * 30 items here:
		 * starts with 2003 - that'll allow up to 30 years to store,
		 * to the year 2033
		 * WATCHOUT! we are making up all those URLS!!!
		 */
		
		Date d = new Date();
		
		// adding years
		// this is how the getYear works - it returns year starting from 1900
		int k = (d.getYear() + 1900) - START_YEAR, 
			i = START_YEAR;		// entries index
		
		if (k > NUM_ITEMS)
		{
			i = START_YEAR + (k - NUM_ITEMS) + 1;
			k = NUM_ITEMS - 1;
		}	
		
		mItems = new ArchiveDropListItem[k + 1];
		
		Log.i(mLogSs, "Created " + k + " items, starting with year " + i);
		
		for ( ; i <= (d.getYear() + 1900) && k >= 0; ++i, --k)
		{
			Log.i(mLogSs, "Created items " + k + " for year " + i);
			mItems[k] = new ArchiveDropListItem(parent, k);
			String year = Integer.valueOf(i).toString();
			mItems[k].setText(year);
			//mItems[k].setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			mItems[k].setUrl("http://integrallife.com/archive/"+year);
			mItems[k].setOnClickListener(new ArchiveItemOnClick());
		}
		
		// initialize the parsers
		int[] tags = new int[1];
		tags[0] = PageParser.CONTENT_AREA;
		mParser = new PageParser(tags);
		
		// utility one - looks for <div> tags
		tags[0] = PageParser.DIV;
		mUtilityParser = new PageParser(tags);
		
        dnldTask = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_ITEMS_LIST);
        dnldTask.execute("http://integrallife.com/archive/by-month-all");
	}

	@Override
	public void Update(CharSequence update) {
		// TODO Auto-generated method stub

	}
	
    /**
     * Process - delivers content read from the input stream to the subscriber.
     * This one can be called from a non-UI thread
     * @param s - a piece of content
     * @return - weather or not the UI thread has to be notified with progress
     */
	@Override
	public boolean Process(String s) {
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Finalize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void UpdateFinal() {
		// TODO Auto-generated method stub

	}

}
