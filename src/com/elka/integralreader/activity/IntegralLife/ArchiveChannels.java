package com.elka.integralreader.activity.IntegralLife;


import com.elka.integralreader.activity.Items.ItemsListActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;


public class ArchiveChannels extends ArchiveItems {
	
	private static final String[] channelsUrls = {
		"http://integrallife.com/ken-wilber-dialogues",
		"http://integrallife.com/loft-series",
		"http://integrallife.com/integral-post",
		"http://integrallife.com/daily-evolver",
		"http://integrallife.com/future-buddhism",
		"http://integrallife.com/future-christianity",
		"http://integrallife.com/book-shelf",
		"http://integrallife.com/art-galleries",
		"http://integrallife.com/stuart-davis-show"
	};
	
	private static final String[] channelsNames = {
		"Ken Wilber Dialogues",
		"The Loft Series",
		"Integral Post",
		"The Daily Evolver",
		"The Future of Buddhism",
		"The Future of Christianity",
		"The Book Shelf",
		"Art Galleries",
		"The Stuart Davis Show"
	};
	
	/**
	 * Passes the URL opening to the ItemsListActivity.
	 * This may not work for all lists,
	 * so need to make different listeners for different types.
	 * @author homyak
	 *
	 */
	private class ArchiveItemOnClick implements View.OnClickListener
	{
		@Override
		public void onClick(View v) {
			if (v.getClass() == ArchiveDropListItem.class)
			{
		        Intent i = new Intent(mParent, ItemsListActivity.class);
		        i.putExtra("ACTION_URI", ((ArchiveDropListItem)v).getUrl());
		        mParent.startActivityForResult(i, 0);
		    	Log.w(mLogSs, "onClickAllContent");
			}
		}
	}
	
	public ArchiveChannels(int type, FragmentActivity parent) {
		super(type, parent);
		
		/*
		 * 9 channels are currently available
		 */
		mItems = new ArchiveDropListItem[9];
		
		for (int i = 0; i < mItems.length; ++i)
		{
			mItems[i] = new ArchiveDropListItem(parent, i);
			mItems[i].setUrl(channelsUrls[i]);
			mItems[i].setText(channelsNames[i]);
			mItems[i].setOnClickListener(new ArchiveItemOnClick());
		}
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public void Update(CharSequence update) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean Process(String s) {
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Finalize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void UpdateFinal() {
		// TODO Auto-generated method stub

	}

}
