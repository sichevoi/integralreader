package com.elka.integralreader.activity.IntegralLife;

import java.util.Vector;

import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.utils.ILRTypes;
import com.elka.integralreader.utils.MyUtils;
import com.elka.integralreader.utils.PageParser;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FeaturedItem
{
	TextView mView;
	Boolean  mOnScreen;
	Boolean  mInitialized;
	String 	 mText;
	String 	 mPath;
	String 	 mRaw;
	Drawable mImage;
	ImageView mImageView;
	ILRTypes.MediaType mType;
	//ViewGroup mViewGroup;
	String mLogSs = "FEATURED_ITEM";
	/**
	 * This is an on-click listener for the image
	 * @author homyak
	 *
	 */
	class ImageOnClick implements View.OnClickListener
	{
		FeaturedItem mParentItem;
		public ImageOnClick(FeaturedItem parent)
		{
			mParentItem = parent;
		}
		@Override
		public void onClick(View v) {
			/**
			 * TODO: crate a new fragment here
			 */
			Log.i("ImageOnClick", "ImageOnClick clicked");
		}
	}
	public FeaturedItem(TextView view, ImageView img, ViewGroup v)
	{
		mView = view;
		mOnScreen = false;
		mInitialized = false;
		mImageView = img;
		mImage = null;
		mPath = null;
		mRaw = null;
		//mViewGroup = v;
		
		/**
		 * Set the image view clickable
		 */
		mImageView.setOnClickListener(new ImageOnClick(this));
	}
	
	public void setRaw(String r)
	{
		mRaw = r;
		mPath = MyUtils.getLink(r);
	}
	
	public void init()
	{
		if (!mInitialized)
		{
			Spanned sp = Html.fromHtml(mRaw, DownloadContentTask.IlImageGetterGet(), null);
			mText = extractTitle(mRaw);
			Log.v(mLogSs, "Converted to text " + mText + " end of text");
			URLSpan[] spans = sp.getSpans(0, sp.length()-1, URLSpan.class);
			for (int i = 0; i < spans.length; ++i)
			{
				Log.d(mLogSs,"Found URLSpan " + spans[i].getURL().toString());
				mPath = "http://integrallife.com/"+spans[i].getURL().toString();
			}
			ImageSpan[] imspans = sp.getSpans(0, sp.length()-1, ImageSpan.class);
			if (imspans.length > 0)
			{
				mImage = imspans[0].getDrawable();
			}
			mInitialized=true;
			mType = MyUtils.getContentType(mRaw);
			Log.v(mLogSs, "Content type is " + mType);
		}
	}
	private String extractTitle(String s)
	{
        int[] arr = new int[1];
        arr[0] = PageParser.ROW_TITLE;
		PageParser parser = new PageParser(arr);
		Vector<String> ret = parser.add(s);
		if (ret.size() > 0)
		{
			return Html.fromHtml(ret.elementAt(0)).toString().replace("\n", "").toUpperCase();
		}
		else
		{
			Log.e(mLogSs, "Can't find anything in views-filed-title!!!");
		}
		return null;
	}
	
	public void show()
	{
		if (!mOnScreen)
		{
			if (mImage != null)
				mImageView.setImageDrawable(mImage);
			if (mText != null)
				mView.setText(mText);
			mOnScreen = true;
		}			
	}
}