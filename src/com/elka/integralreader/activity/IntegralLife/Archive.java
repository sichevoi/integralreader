package com.elka.integralreader.activity.IntegralLife;

import java.util.Vector;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.actionbarsherlock.app.SherlockFragment;
import com.elka.integralreader.download.DownloadContentTask;
import com.elka.integralreader.download.DownloadSubscriber;
import com.elka.integralreader.utils.PageParser;
import com.elka.integralreader.R;

/**
 * This activity is intended to provide access to different type of content:
 * - All
 * - Articles
 * - Channels
 *   
 */
public class Archive extends SherlockFragment implements DownloadSubscriber
{
    private static final int ITEMS_LIST = 0;

	Vector<FeaturedItem> mFeatured;
	DownloadContentTask dnldTask;
	PageParser mParser;
	ProgressDialog mDialog;
	int mNumItems;
	ViewGroup mContainer;
	String mLogSs = "ARCHIVE";
	Boolean allInPlace;

	/**
	 * TODO: this one is not the best practice. Eventually would prefer
	 * to go to some sort of state preserving instead.
	 */
	View mView;
	
	public static final int MAX = 4;
	public static final int TIMELINE = 3;
	public static final int CONTRIBS = 2;
	public static final int TOPICS = 1;
	public static final int CHANNELS = 0;
	
	public Archive()
	{
		Log.d(mLogSs, "Constructor called");
		allInPlace = false;
	}

    /** 
     * Called when the activity is first created. 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(mLogSs, "onCreate called");
        
    	mFeatured = new Vector<FeaturedItem>(10);

        int[] arr = new int[1];
        arr[0] = PageParser.CAROUSEL_ITEM;
        mParser = new PageParser(arr);
        
        //mDialog = ProgressDialog.show(Archive.this, "", "Loading...", true);
        
        mNumItems = 0;
        dnldTask = new DownloadContentTask((DownloadSubscriber)this, DownloadContentTask.TYPE_GALLERY);
        // FIXME: uncomment
        //dnldTask.execute("http://integrallife.com/content");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    	super.onCreateView(inflater, container, savedInstanceState);
    	if (mView == null)
    	{
    		Log.d(mLogSs, "onCreateView called");
    		mContainer = container;
    		mView = inflater.inflate(R.layout.archive, container, false);
    	}
    	else
    	{
    		((ViewGroup)mView.getParent()).removeView(mView);
   			Log.i(mLogSs, "Removed from container " + mContainer + " added to " + container);
   			if (mContainer != container)
   			{
   				allInPlace = false;
   				mContainer = container;
   			}   		
    	}
    	
    	return mView;
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	Log.d(mLogSs, "onStart called");
    	
    	/**
    	 * We can only start getting these views here, 
    	 * because in the onCreateView the view is not yet ready
    	 */
    	if (false == allInPlace)
    	{
    		// TODO: rework to work through the SimpleCursorAdapter
    		// see http://stackoverflow.com/questions/5730035/cursoradapter-slow-jerky-scroll
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext1), 
	        		(ImageView) getView().findViewById(R.id.acrimg1), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext2), 
	        		(ImageView) getView().findViewById(R.id.acrimg2), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext3), 
	        		(ImageView) getView().findViewById(R.id.acrimg3), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext4), 
	        		(ImageView) getView().findViewById(R.id.acrimg4), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext5), 
	        		(ImageView) getView().findViewById(R.id.acrimg5), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext6), 
	        		(ImageView) getView().findViewById(R.id.acrimg6), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext7), 
	        		(ImageView) getView().findViewById(R.id.acrimg7), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext8), 
	        		(ImageView) getView().findViewById(R.id.acrimg8), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext9), 
	        		(ImageView) getView().findViewById(R.id.acrimg9), mContainer));
	        mFeatured.add(new FeaturedItem((TextView) getView().findViewById(R.id.arctext10), 
	        		(ImageView) getView().findViewById(R.id.acrimg10), mContainer));
	        allInPlace = true;
    	}
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	Log.d(mLogSs, "onPause called");	
    }
    
    public void onActivityCreated(Bundle savedInstanceState)
    {
    	super.onActivityCreated(savedInstanceState);
    	Log.d(mLogSs, "onActivityCreated called");	
    }
    
    public void onDestroyView()
    {
    	super.onDestroyView();
    	allInPlace = false;
    	mNumItems = 0;
    	mFeatured.clear();
    	Log.d(mLogSs, "onDestroyView called");	
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case ITEMS_LIST:
                /* TODO: how shall I handle this? */
                break;
            default:
                break;
        }
    }
   

    /**
     * DownloadSubscriber interface implementation
     */
	@Override
	public void Update(CharSequence update) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean Process(String s) {
		Vector<String> parsedTags = mParser.add(s);
		Log.i(mLogSs, "Found " + parsedTags.size() + " slideshow tags");

		for (int j = 0; (j < parsedTags.size()) && (j < mFeatured.size()); ++j)
		{			
			Log.v(mLogSs, "Adding a new raw element " + parsedTags.elementAt(j));
	        	mFeatured.elementAt(mNumItems).setRaw(parsedTags.elementAt(j));
	        	++mNumItems;
		}
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Finalize() {
		for (int i = 0; i < mNumItems; ++i)
		{
			mFeatured.elementAt(i).init();
		}	
	}

	@Override
	public void UpdateFinal() {
    	if (mDialog != null)
    	{
    		mDialog.cancel();
    		mDialog = null;
    	}
		for (int i = 0; i < mNumItems; ++i)
		{
			mFeatured.elementAt(i).show();
		}
	}
}