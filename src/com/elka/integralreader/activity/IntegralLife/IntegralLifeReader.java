package com.elka.integralreader.activity.IntegralLife;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.elka.integralreader.activity.Items.ItemsListActivity;
import com.elka.integralreader.download.images.ImageProvider;
import com.elka.integralreader.R;

/**
 * @author homyak
 * IntegralLifeReader - starter activity for the app.
 * Manages top level fragments.
 *
 */
public class IntegralLifeReader extends SherlockFragmentActivity
{

	SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;
	private String mLogSs = "IntegralLifeReader";
	private ArchiveStateMachine state;
	
	/**
	 * Class, supporting the fold-unfold state machine
	 */
	private class ArchiveStateMachine {
				
		private String mLogSs = "ArchiveStateMachine";
		
		private ArchiveItems[]		items;
		// need this to reinit the archive items if the view has changed.
		private View[]				views;
		//private Boolean[]			inits;
		
		private int 				nowVisible;
		FragmentActivity mParent;
		
		public ArchiveStateMachine(FragmentActivity parent) {
			
			mParent = parent;
			views = new View[Archive.MAX];
			items = new ArchiveItems[Archive.MAX];
			for (int i = 0; i < items.length; ++i)
			{
				items[i] = ArchiveItems.getItem(i, parent);
			}

			nowVisible = Archive.MAX;
		}

		/**
		 * View t will be set to visible, all others to gone
		 * @param t
		 */
		public void toggleVisible(int t)
		{
			Log.v(mLogSs, "toggleVisible called for " + t);
			if (t < items.length)
			{
				ArchiveItems ai = items[t];
				if ( ai!= null)
				{
					/**
					 * First - set to gone if others are visible.
					 * nowVisible will indicate that
					 */
					if (nowVisible < items.length)
					{
						Log.i(mLogSs, "set views invisible " + t);
						items[nowVisible].setInvisible();
					}
					
					/**
					 * Next - set the requested view visible.
					 * Only if it's not the previous visible.
					 */
					if (nowVisible != t)
					{
						Log.i(mLogSs, "set views visible " + t);
						ai.setVisible();
						nowVisible = t;
					}
					else // toggled the current visible to invisible - reset the nowVisible
					{
						Log.i(mLogSs, "not doing more - that was the current visible " + t);
						nowVisible = Archive.MAX;
					}
				}
			}
		}
		
		/**
		 * Fill the liner layout
		 * If the view is not filled yet with data - fill it.
		 * TODO: move it to the proper place
		 */
		public void layoutInit(int t, LinearLayout ll, ScrollView sv, RelativeLayout rl, View currView)
		{
			Log.v(mLogSs, "layoutInit called for " + t);
			if ((t < items.length))
			{
				if (views[t] != null)
				{
					if (!views[t].equals(currView))
					{
						// need to reinitialize the archive item
						items[t] = ArchiveItems.getItem(t, mParent);
						views[t] = currView;
					}
				}
				Log.i(mLogSs, "Initializing the views for " + t);
				items[t].initLayout(ll, sv, rl);
				//inits[t] = true;
			}
		}
	}
	
    /** Called when the activity is first created. */
        @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        if(null != mViewPager)
        {
        	mViewPager.setAdapter(mSectionsPagerAdapter);
        }
        else
        {
        	Log.e(mLogSs, "Could not find view");
        }
        
        state = new ArchiveStateMachine(this);
    }
        
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main_menu, menu);

        /* set up a listener for the refresh item
        final MenuItem refresh = (MenuItem) menu.findItem(R.id.menu_refresh);
        refresh.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            // on selecting show progress spinner for 1s
            public boolean onMenuItemClick(MenuItem item) {
                // item.setActionView(R.layout.progress_action);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refresh.setActionView(null);
                    }
                }, 1000);
                return false;
            }
        });*/
        return super.onCreateOptionsMenu(menu);
    }
        
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
        	Log.v("SectionsPagerAdapter", "getItem called for item " + i);
        	Fragment fragment = null;
        	switch (i)
        	{
        	case 0:
        		fragment = new Archive();
        		break;
        		
        	case 1:
        		fragment = new StubFragment();
        		break;
        		
        	case 2:
        		fragment = new StubFragment();
        		break;
        		
        	case 3:
        		fragment = new LatestGreatest();
        		break;      
        		
        	case 4:
        		fragment = new StubFragment();
        		break;   
        		
        	case 5:
        		fragment = new StubFragment();
        		break; 
        		
    		default:
        		fragment = new StubFragment();
        		break; 
        	}
        				
            return fragment;
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
            case 0:
            	return getString(R.string.title_section0).toUpperCase();
            	
            case 1: 
            	return getString(R.string.title_section1).toUpperCase();
                
            case 2: 
            	return getString(R.string.title_section2).toUpperCase();
            	
            case 3:
            	return getString(R.string.title_section3).toUpperCase();
            	
            case 4:
            	return getString(R.string.title_section4).toUpperCase();
            	
            case 5:
            	return getString(R.string.title_section5).toUpperCase();
                
            }
            return null;
        }
    }
    
    /**
     * Text views' on-click listeners for
     * all content, articles and galleries items
     * 
     * @author homyak
     *
     */
    public void onClickAllContent(View v)
    {
        Intent i = new Intent(IntegralLifeReader.this, ItemsListActivity.class);
        i.putExtra("ACTION_URI", "all-content");
        //i.putExtra("ACTION_URI", "http://integrallife.com/book-shelf");
        startActivityForResult(i, 0);
    	Log.w(mLogSs, "onClickAllContent");
    }
    
    public void onClickFreeItems(View v)
    {
        Intent i = new Intent(IntegralLifeReader.this, ItemsListActivity.class);
        i.putExtra("ACTION_URI", "http://integrallife.com/v/search?keys=&tid=All&tid_2=All&tid_1=201&tid_3=All");
        startActivityForResult(i, 0);
    	Log.w(mLogSs, "onClickFreeItems");
    }    
    
    public void onClickChannels(View v)
    {
    	Log.w(mLogSs, "onClickChannels");
    	state.layoutInit(Archive.CHANNELS, 
    			(LinearLayout)findViewById(R.id.channels_list_lo),
    			(ScrollView)findViewById(R.id.channels_list),
    			(RelativeLayout)findViewById(R.id.channels_lo), v);
    	
    	state.toggleVisible(Archive.CHANNELS);
    }    
    
    public void onClickTopics(View v)
    {
    	Log.i(mLogSs, "onClickTopics");
    	state.layoutInit(Archive.TOPICS, 
    			(LinearLayout)findViewById(R.id.topics_list_lo),
    			(ScrollView)findViewById(R.id.topics_list),
    			(RelativeLayout)findViewById(R.id.topics_lo), v);
    	
    	state.toggleVisible(Archive.TOPICS);
    }
    
    public void onClickContrib(View v)
    {
    	Log.i(mLogSs, "onClickContrib");
    	state.layoutInit(Archive.CONTRIBS, 
    			(LinearLayout)findViewById(R.id.contrib_list_lo),
    			(ScrollView)findViewById(R.id.contrib_list),
    			(RelativeLayout)findViewById(R.id.contrib_lo), v);
    	
    	state.toggleVisible(Archive.CONTRIBS);
    }
    
    public void onClickTimeLine(View v)
    {
    	Log.i(mLogSs, "onClickTimeLine");
    	state.layoutInit(Archive.TIMELINE, 
    			(LinearLayout)findViewById(R.id.timeline_list_lo),
    			(ScrollView)findViewById(R.id.timelineb_list),
    			(RelativeLayout)findViewById(R.id.timeline_lo), v);
    	
    	state.toggleVisible(Archive.TIMELINE);    	
    }
}