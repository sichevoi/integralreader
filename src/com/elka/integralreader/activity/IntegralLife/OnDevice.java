package com.elka.integralreader.activity.IntegralLife;

import com.actionbarsherlock.app.SherlockFragment;

import android.os.Bundle;

/**
 * This activity is intended to provide user access to the content
 * downloaded and stored on device.
 * This is going to be a high level view incorporating the following items:
 * - Manage
 * - List of content (the actual items available)
 * @author homyak
 *
 */
public class OnDevice extends SherlockFragment {
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
