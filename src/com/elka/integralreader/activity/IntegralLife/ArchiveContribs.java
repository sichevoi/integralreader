package com.elka.integralreader.activity.IntegralLife;

import com.elka.integralreader.activity.Items.ItemsListActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

/**
 * Will represent the list of contributors:
 * Teachers
 * Leaders
 * Artists
 * Scholars
 * Featured
 * All 
 * @author homyak
 *
 */
public class ArchiveContribs extends ArchiveItems {

	private static final String[] contribsUrls = {
		"http://integrallife.com/contributors-teachers",
		"http://integrallife.com/contributors-leaders",
		"http://integrallife.com/contributors-artists",
		"http://integrallife.com/contributors-scholars",
		"http://integrallife.com/contributors",
		"http://integrallife.com/contributors-all"
	};
	
	private static final String[] contribsTitles = {
		"Teachers",
		"Leaders",
		"Artists",
		"Scholars",
		"Featured",
		"All"
	};
	
	/**
	 * Passes the URL opening to the ItemsListActivity.
	 * This may not work for all lists,
	 * so need to make different listeners for different types.
	 * @author homyak
	 *
	 */
	private class ArchiveItemOnClick implements View.OnClickListener
	{
		@Override
		public void onClick(View v) {
			if (v.getClass() == ArchiveDropListItem.class)
			{
		        Intent i = new Intent(mParent, ItemsListActivity.class);
		        i.putExtra("ACTION_URI", ((ArchiveDropListItem)v).getUrl());
		        mParent.startActivityForResult(i, 0);
		    	Log.w(mLogSs, "onClickAllContent");
			}
		}
	}
	
	public ArchiveContribs(int type, FragmentActivity parent) {
		super(type, parent);
		/*
		 * 6 entries in the contributors types
		 */
		mItems = new ArchiveDropListItem[6];
		
		for (int i = 0; i < mItems.length; ++i)
		{
			mItems[i] = new ArchiveDropListItem(parent, i);
			//mItems[i].setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			mItems[i].setUrl(contribsUrls[i]);
			mItems[i].setText(contribsTitles[i]);
			mItems[i].setOnClickListener(new ArchiveItemOnClick());
		}
		
	}

	@Override
	public void Update(CharSequence update) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean Process(String s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Process(Drawable img, String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Finalize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void UpdateFinal() {
		// TODO Auto-generated method stub

	}

}
