package com.elka.integralreader.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.util.Log;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 01.05.12
 * Time: 1:24
 * To change this template use File | Settings | File Templates.
 */
@SuppressLint("NewApi")
@TargetApi(9)
public class PageParser
{
    ArrayDeque<Integer> mOpenTags;
    ArrayDeque<Integer> mClosedTags;
    Boolean         mStartTag;
    Boolean         mComment;
    StringBuilder   mCurrTag;		/* a string builder containing the full current tag */
    StringBuilder   mFollowedTag;
    String          mFullTag;
    List<Integer>   mTagsToFollow;   /* keeps the list of tags which do not require to be parsed inside*/
    
    Boolean tmpInStyle;
    
    private static final String mLogSs = "PAGE_PARSER";

    /**
     * TODO: add item list and pager
     * <div class="item-list">
     * <ul class="pager">
     */
    public static final int DIV_ID_CONTENT = 0; 	//"<div id=\"content\""
    public static final int DIV_CLASS_ABSTRACT = 1;	//"<div class=\"abstract\""
    public static final int DIV_CLASS_IMAGE = 2;	//"<div class=\"image\""
    public static final int SCRIPT = 3;				
    public static final int LINK = 4;
    public static final int STYLE = 5;
    public static final int HTML = 6;
    public static final int HEAD = 7;
    public static final int TITLE = 8;
    public static final int META = 9;
    public static final int DIV_ID_TOP_NAV = 10;	//"<div id=\"top-nav\""
    public static final int LI = 11;
    public static final int FORM = 12;
    public static final int INPUT = 13;
    public static final int A_HREF = 14;
    public static final int DIV_CLASS_SUBMITTED = 15;	//"<div class=\"submitted"
    public static final int DIV_CLASS_ICON = 16;		//"<div class=\"asset_icon"
    public static final int IMG = 17;
    public static final int DIV_OTHER = 18;
    public static final int UL_TAG = 19;
    public static final int BODY = 20;
    public static final int FORMAT_TAG = 21;
    public static final int OPTION = 22;
    public static final int SELECT = 23;
    public static final int BR = 24;
    public static final int LABEL = 25;
    public static final int SPAN = 26;
    public static final int OPTGROUP = 27;
    public static final int DIV_CLASS_NODE_TEASER = 28;	//"<div class=\"node node-teaser"
    public static final int TABLES = 29;
    public static final int OBJECT = 30;
    public static final int IFRAME = 31;
    public static final int MAIN_TABLE = 33;
    public static final int VIEW_ROW = 34;		//<div class="views-row
    public static final int ROW_TITLE = 35;		//<div class="views-field-title
    public static final int ROW_TID1 = 36;		//<div class="views-field-tid-1
    public static final int ROW_TID2 = 37;		//<div class="views-field-tid-2
    public static final int DIV = 38;			//<div>
    public static final int CENTER = 39;
    public static final int VIEW_TID = 40;		//<div class="views-field-tid">
    public static final int NODE_TYPE = 41;		//<a href="/node-type/ 
    public static final int PAGER = 42;			//<li class="pager-
    public static final int TABLES_TD = 43;     //<td for image galleries
    public static final int VIEW_SLIDESHOW = 44; //<div class="views_slideshow
    public static final int VIEWS_TITLE = 45;	//<div class="views-field-title
    public static final int CLASS_TYPE = 46;	//<div class="type
    public static final int CAROUSEL_ITEM = 47; //<li class="jcarousel-item
    public static final int CONTENT_AREA = 48;  //<div id="content-area"
    public static final int META_TITLE = 49;
    
    private static final int UNKNOWN = 60;

    private static final int INVALID_TAG = -1;

    public PageParser(int[] tagToFollow)
    {
        mOpenTags = new ArrayDeque<Integer>();
        mClosedTags = new ArrayDeque<Integer>();
        mStartTag = false;
        mComment = false;
        mCurrTag = new StringBuilder();
        mTagsToFollow = new ArrayList<Integer>(tagToFollow.length);
        for (int i = 0; i < tagToFollow.length; i++ )
        {
            mTagsToFollow.add(tagToFollow[i]);
            Log.v(mLogSs, "requested to follow tag "+tagToFollow[i]);
        }
        mFollowedTag = null;
        mFullTag = null;
    }

    public void reset()
    {
        mStartTag = false;
        mComment = false;
        mCurrTag = new StringBuilder();
        mFollowedTag = null;
        mFullTag = null;    	
    }
    
    /**
     * @param str
     */

    public Vector<String> add(String str)
    {
        Vector<String> tags = new Vector<String>();
        Log.v(mLogSs, str);
        for (int i = 0; i < str.length(); ++i)
        {
            if (mStartTag)
            {
                mCurrTag.append(str.charAt(i));
            }

            if ((mFollowedTag != null) && !mComment)
            {
                mFollowedTag.append(str.charAt(i));
            }

            if (str.charAt(i) == '<')
            {
                if (mStartTag)
                {
                    //Log.e(mLogSs, "Huh! a tag is started when another one ongoing: " + str.substring(i, ((str.length() - i) > 10) ? (i + 9) : ( i + (str.length() - i - 1)) ));
                    //Log.e(mLogSs, "The previous tag is " + mCurrTag.toString());
                    if (mCurrTag.toString().startsWith("<!--") && false == tmpInStyle)
                    {
                        mComment = true;
                        Log.v(mLogSs, "Opening comment braces found");
                    }
                }
                mStartTag = true;
                mCurrTag = new StringBuilder();
                mCurrTag.append("<");
            }

            if (str.charAt(i) == '>')
            {
                mStartTag = false;
                if (!mComment)
                {
                    if (null != tagIdentify(mCurrTag.toString()))
                    {
                        tags.add(mFullTag);
                        mFullTag = null;
                    }

                }
                else
                {
                    if (mCurrTag.toString().contains("-->"))
                    {
                        Log.v(mLogSs, "Closing comment braces found");
                        mComment = false;
                    }
                }
            }
        }

        Log.v(mLogSs, "Returning " + tags.size() + " tags");
        
        return tags;
    }

    String tagIdentify(String s)
    {
        int tag = INVALID_TAG;
        Boolean closing = false;
        if (s.startsWith("</"))
        {
            closing = true;
            Log.v(mLogSs, "Closing " + s + " tag found");
        }
        else if (s.startsWith("<div id=\"content\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "DIV_ID_CONTENT");
           tag = DIV_ID_CONTENT;
        }
        else if (s.startsWith("<div class=\"views-row"))
        {
            //Log.v(mLogSs, "New tag identified: " + "VIEW_ROW");
            tag = VIEW_ROW;
        }
        else if (s.startsWith("<div class=\"views-field-title\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "ROW_TITLE");
            tag = ROW_TITLE;
        }
        else if (s.startsWith("<div class=\"views-field-tid-1\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "ROW_TID1");
            tag = ROW_TID1;
        }
        else if (s.startsWith("<div class=\"views-field-tid-2\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "ROW_TID2");
            tag = ROW_TID2;
        }
        else if (s.startsWith("<div class=\"views-field-tid\""))
        {
        	tag = VIEW_TID;
        }
        else if (s.startsWith("<div class=\"views_slideshow"))
        {
        	tag = VIEW_SLIDESHOW;
        }
        else if (s.startsWith("<a href=\"/node-type/") || s.startsWith("<a href=\"/taxonomy/term/243"))
        {
        	//Log.v(mLogSs, "found node-type");
        	tag = NODE_TYPE;
        }
        else if (s.startsWith("<li class=\"jcarousel-item"))
        {
        	tag = CAROUSEL_ITEM;
        }
        else if (s.startsWith("<li class=\"pager-"))
        {
        	//Log.v(mLogSs, "Pager found");
        	tag = PAGER;
        }
        else if (s.startsWith("<div class=\"image\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "DIV_CLASS_IMAGE");
            tag = DIV_CLASS_IMAGE;
        }
        else if (s.startsWith("<div class=\"abstract\""))
        {
            //Log.v(mLogSs, "New tag identified: " + "DIV_CLASS_ABSTRACT");
            tag = DIV_CLASS_ABSTRACT;
        }
        else if (s.startsWith("<script"))
        {
            //Log.v(mLogSs, "New tag = " + "SCRIPT") ;
        	tmpInStyle = true;
            tag = SCRIPT;
        }
        else if (s.startsWith("<div class=\"node node-teaser"))
        {
            tag = DIV_CLASS_NODE_TEASER;
        }
        else if (s.startsWith("<link"))
        {
            tag = LINK;
        }
        else if (s.startsWith("<div class=\"submitted"))
        {
            tag = DIV_CLASS_SUBMITTED;
        }
        else if (s.startsWith("<div class=\"asset_icon"))
        {
            tag = DIV_CLASS_ICON;
        }
        else if (s.startsWith("<div class=\"type"))
        {
        	tag = CLASS_TYPE;
        }
        else if (s.startsWith("<div id=\"top-nav\""))
        {
            tag = DIV_ID_TOP_NAV;
        }
        else if (s.equals("<div>"))
        {
            tag = DIV;
            Log.v(mLogSs, "<div> tag found");
        }
        else if (s.startsWith("<div id=\"content-area\""))
        {
        	tag = CONTENT_AREA;
        	Log.d(mLogSs,"CONTENT_AREA tag found");
        }
        else if (s.startsWith("<div"))
        {
            tag = DIV_OTHER;
        }
        else if (s.startsWith("<ul"))
        {
            tag = UL_TAG;
        }
        else if (s.contains("<center>"))
        {
            tag = CENTER;
        }
        else if (s.startsWith("<!"))
        {
            Log.v(mLogSs, "Skipping non tag " + s);
        }
        else if (s.startsWith("<img"))
        {
        	Log.v(mLogSs, "Found the image tag");
            tag = IMG;
        }
        else if (s.startsWith("<select"))
        {
            tag = SELECT;
        }
        else if (s.contains("style"))
        {
            tag = STYLE;
        }
        else if (s.startsWith("<html"))
        {
            tag = HTML;
        }
        else if (s.startsWith("<head"))
        {
            tag = HEAD;
        }
        else if (s.startsWith("<title"))
        {
            tag = TITLE;
        }
        else if (s.startsWith("<meta name=\"title\""))
        {
        	tag = META_TITLE;
        }
        else if (s.startsWith("<meta"))
        {
            tag = META;
        }
        else if (s.startsWith("<li"))
        {
            tag = LI;
        }
        else if (s.startsWith("<form "))
        {
            tag = FORM;
        }
        else if (s.startsWith("<input "))
        {
            tag = INPUT;
        }
        else if (s.startsWith("<a"))
        {
            tag = A_HREF;
        }
        else if (s.startsWith("<body "))
        {
            tag = BODY;
        }
        else if (s.startsWith("<option"))
        {
            tag = OPTION;
        }
        else if (s.startsWith("<p") || s.startsWith("<h2") || s.startsWith("<h3") || s.startsWith("<sup>")
                || s.startsWith("<em>") || s.startsWith("<strong>") || s.startsWith("<b>") || s.startsWith("<h1")
                || s.startsWith("<font"))
        {
            tag = FORMAT_TAG;
        }
        else if (s.startsWith("<td"))
        {
        	tag = TABLES_TD;
        }
        else if (s.startsWith("<tr") || s.startsWith("<thead")
                || s.startsWith("<th>") || s.startsWith("<tbody"))
        {
            tag = TABLES;
        }
        else if (s.startsWith("<table"))
        {
            tag = MAIN_TABLE;
        }
        else if (s.startsWith("<br"))
        {
            tag = BR;
        }
        else if (s.startsWith("<optgroup"))
        {
            tag = OPTGROUP;
        }
        else if (s.startsWith("<span"))
        {
            tag = SPAN;
        }
        else if (s.startsWith("<label"))
        {
            tag = LABEL;
        }
        else if (s.startsWith("<object"))
        {
            tag = OBJECT;
        }
        else if (s.startsWith("<iframe"))
        {
            tag = IFRAME;
        }
        else
        {
            tag = UNKNOWN;
            Log.v(mLogSs, "Undefined tag " + s);
        }

        //Log.v(mLogSs, "Handling tag " + s);

        if (closing)
        {
            if (!mOpenTags.isEmpty())
            {
                tag = mOpenTags.pop();
                Log.v(mLogSs, "For " + s + " popped " + tag + " tag out of the OpenTags, " + mOpenTags.size() + " left");
                if (tag == STYLE)
                {
                	tmpInStyle = false;
                }
            }
            else
            {
                Log.e(mLogSs, "Huh!!! the Open tags array is empty!!!!! " + s);
            }

        }
        if (tag != INVALID_TAG)
        {
            if (closing || !tagHasPair(tag))
            {
                //Log.v(mLogSs, "Pushing " + tag + " tag into the ClosedTags");
                if (mTagsToFollow.contains(tag))
                {
                    if (mFollowedTag != null)
                    {
                        mFullTag = mFollowedTag.toString();
                        mFollowedTag = null;
                    }
                    /* A special case - we parse the full tag at once in case of no-closing tags */
                    else if (!tagHasPair(tag))
                    {
                    	mFullTag = s;
                    }
                    else
                    {
                        //Log.v(mLogSs, "mFollowedTag equals 0 for tag " + tag);
                    }
                }
                mClosedTags.push(tag);
            }
            else
            {
                Log.v(mLogSs, "Pushing " + tag + " tag into the OpenTags");
                if (mTagsToFollow.contains(tag))
                {
                	Log.v(mLogSs, "found a new tag to follow, creating a new string builder");
                	if (mFollowedTag != null)
                	{
                		Log.w(mLogSs, "found a different tag to folow ("+tag+") while another is in progress: " + mFollowedTag.toString());
                	}
                    mFollowedTag = new StringBuilder();
                    mFollowedTag.append(s);
                }
                mOpenTags.push(tag);
            }
        }

        return mFullTag;
    }

    Boolean tagHasPair(int tag)
    {
        Boolean pair = true;
        switch (tag)
        {
            case LINK:
            case INPUT:
            case META_TITLE:
            case META:
            //case A_HREF:
            case BR:
            case IMG:
                pair = false;
                break;
            default:
                break;
        }
        return pair;
    }

}
