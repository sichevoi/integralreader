package com.elka.integralreader.utils;

public interface TransactionListener {
	void onStartTransaction(ILRTypes.MediaType type, String path);
}
