package com.elka.integralreader.utils;

import android.util.Log;

public class MyUtils {

	/**
	 * Gets the base path from the url.
	 * @param url
	 * @return
	 */
	public static String getPath(String url)
	{
		if (url == null)
			return null;
		StringBuilder path = new StringBuilder();
		String s[] = url.split("/");
		for (int i = 0; i < (s.length - 1); ++i)
		{
			path.append(s[i]);
			path.append('/');
		}
		
		return path.toString();
	}
	
	/**
	 * Gets one - and only first one - link from the passed in string with tags.
	 * Makes a search for "<a href=" tag.
	 * @param raw
	 * @return
	 */
	public static String getLink(String raw)
	{
		String path = null;
        if (raw.contains("<a href=\"/"))
        {
            String[] refs = raw.split("<a href=\"/", 2)[1].split("</a>");

            if(refs[0].contains("\">"))
            {
                String[] s1 = refs[0].split("\">");
                if (s1.length == 2)
                {
                    if (!s1[1].startsWith("&"))
                    {
                    	path = s1[0];	                                	
                    }
                }
            }
        }
        
        return path;
	}
	
	/**
	 * To identify the content type preferably need to pass here the full
	 * row list of tags.
	 * @param s
	 * @return
	 */
	public static ILRTypes.MediaType getContentType(String s)
	{
		ILRTypes.MediaType type = ILRTypes.MediaType.UNKNOWN;
		
		if (s.contains("video") ||
				s.contains("loft-series") ||
				s.contains("daily-evolver") ||
				s.contains("ken-wilber-dialogues"))
		{
			type = ILRTypes.MediaType.VIDEO;
		}
		else if (s.contains("integral-post") || 
				s.contains("article"))
		{
			type = ILRTypes.MediaType.ARTICLE;
		}
		else if (s.contains("art-galleries"))
		{
			type = ILRTypes.MediaType.ART_GALLERY;
		}
		else
		{
			Log.e("MyUtils", "Can't identify content type! Passed: " + s);
		}
		return type;
	}
	
}
