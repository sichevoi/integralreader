package com.elka.integralreader.utils;

import java.util.Vector;

/**
 * The resource storage for the app.
 * It'll be managing such things as the runtime downloaded resources access 
 * as well as caching the data on the device storage and retrieving it.
 * This is going to be a singleton.
 * 
 * @author homyak
 *
 */

public class ResourceStorage {
	
	Vector<Vector<CustomResource>> mRes;
	
	private static ResourceStorage instance = new ResourceStorage();
	
	/**
	 * Singleton - private construction
	 */
	private ResourceStorage() {
		
		 mRes = new Vector<Vector<CustomResource>>();
		 for(int i = 0; i < (CustomResource.Type.TYPE_MAX).getInt(); ++i)
		 {
			 mRes.add(new Vector<CustomResource>());
		 }
		
	}
	
	public static ResourceStorage getInstance()
	{
		return instance;
	}
	
	public Vector<CustomResource> getResourceSet(CustomResource.Type type)
	{
		return mRes.elementAt(type.getInt());
	}
	
	/**
	 * Adds a resource to the set.
	 * Return the current count of resources in the set.
	 * @param res
	 * @return
	 */
	public int addToResourceSet(CustomResource res)
	{
		mRes.elementAt(res.getType().getInt()).add(res);
		return mRes.elementAt(res.getType().getInt()).size();
	}
	
	public int clearResourceSet(CustomResource.Type type)
	{
		mRes.elementAt(type.getInt()).clear();
		return 0;
	}
}
