package com.elka.integralreader.utils;

/**
 * The CustomResource interface provides 
 * @author homyak
 *
 */
public interface CustomResource{

	/**
	 * New resources have to be added before the TYPE_MAX and that has to be
	 * incremented. The type is used for direct array indexing in the
	 * ResourceStorage class.
	 * @author homyak
	 *
	 */
	public enum Type{
		TYPE_GALLERY,
		TYPE_ARTICLE,
		TYPE_PAGE,
		TYPE_AUDIO,
		TYPE_VIDEO,
		TYPE_MAX;
		public int getInt()
		{
			switch (this)
			{
			case TYPE_GALLERY:
				return 0;
			case TYPE_ARTICLE:
				return 1;
			case TYPE_PAGE:
				return 2;
			case TYPE_AUDIO:
				return 3;
			case TYPE_VIDEO:
				return 4;
			case TYPE_MAX:
				return 5;
			}
			
			return 0;
		}
	}
	
	/**
	 * Returns an id of the stored resource.
	 */
	public abstract String getId();
	
	public Type getType(); 

}
