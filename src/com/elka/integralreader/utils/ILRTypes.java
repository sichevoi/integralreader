package com.elka.integralreader.utils;

import android.util.Log;

public class ILRTypes {
	
	public enum MediaType
	{
        ARTICLE(0),
        ART_GALLERY(1),
        AUDIO(2),
        VIDEO(3),
        ELEARNING(4),
        SERIES(5),
        UNKNOWN(10);
        
        private final int value;
        private MediaType(int val) { value = val; }
        public int getInt() { return value; }
        public static MediaType fromInt(int val) {
            switch (val) {
            case 0: return ARTICLE;
            case 1: return ART_GALLERY;
            case 2: return AUDIO;
            case 3: return VIDEO;
            case 4: return ELEARNING;
            case 5: return SERIES;
            default:
                Log.e("MediaType", "Requesting unsupported content type value " + val);
                return UNKNOWN;
            }
        }
	}
	
	public static MediaType getContentType(int i)
	{
		MediaType t = MediaType.UNKNOWN;
		switch(i)
		{
		case 0:
			t = MediaType.ARTICLE;
			break;
		case 1:
			t = MediaType.ART_GALLERY;
			break;
		case 2:
			t = MediaType.AUDIO;
			break;
		case 3:
			t = MediaType.VIDEO;
			break;
		case 4:
			t = MediaType.ELEARNING;
			break;
		case 5:
			t = MediaType.SERIES;
			break;
		default:
			break;
		}
		return t;
	}
	
}
