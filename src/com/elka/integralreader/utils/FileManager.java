package com.elka.integralreader.utils;

import java.util.HashMap;
import java.util.Map;

public class FileManager {
    private static Map<String, Object> objects = new HashMap<String, Object>(20);
    
    public static boolean put(String fileName, Object obj) {
        objects.put(fileName, obj);
        return true;
    }
    
    public static Object get(String fileName) {
        return objects.get(fileName);
    }
}
