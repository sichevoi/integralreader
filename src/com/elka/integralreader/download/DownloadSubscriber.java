package com.elka.integralreader.download;

import android.graphics.drawable.Drawable;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 26.03.12
 * Time: 22:44
 * To change this template use File | Settings | File Templates.
 */
public interface DownloadSubscriber
{
    /**
     * Update() method is called within the UI thread and
     * can be used to inform the adapter about the data set
     * changes
     * @param update
     */
    public void Update(CharSequence update);
    //public void Update(Drawable img);

    /**
     * Process - delivers content read from the input stream to the subscriber.
     * This one can be called from a non-UI thread
     * @param s - a piece of content
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(String s);

    /**
     * Process - delivers content read from the input stream to the subscriber.
     * This one can be called from the UI thread
     * @param img - a drawable object to process
     * @param path TODO
     * @return - weather or not the UI thread has to be notified with progress
     */
    public boolean Process(Drawable img, String path);
    /**
     * Finalize - to be called after the content is 0
     */
    public void Finalize();
    
    /**
     * Called during OnPostExecute
     */
    public void UpdateFinal();
}
