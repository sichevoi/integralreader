package com.elka.integralreader.download;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;

import com.elka.integralreader.download.images.ImageResponseHandler;

/**
 * Created by IntelliJ IDEA.
 * User: homyak
 * Date: 26.03.12
 * Time: 0:45
 * The intention of this class is to handle all the required network download activities.
 * It will open a session at the first call, log in and maintain the session.
 * TODO: this functionality should be moved to the ContentProvider and
 * local db caching should be implemented.
 */
@TargetApi(3)
public class DownloadContentTask extends AsyncTask<String, String, CharSequence>
{
    private static final String logSubsystem = "CONTENT_DOWNLOADER";   /**< logging subsystem name */
    private DownloadSubscriber mListener;                      /**< updates listener, must be passed into the constructor */
    private int mType;                                          /**< the type of content to be downloaded */

    public static final int TYPE_ARTICLE = 0;                   /**< the content type is article*/
    public static final int TYPE_ITEMS_LIST = 1;                /**< the content type is items list*/
    public static final int TYPE_GALLERY = 2;
    public static final int TYPE_RAW = 3;
    
    
    private Boolean mbUpdate;
    private static final Object mLock;

    /**
     * Some downloader are below.
     * They are required to be static to keep the session
     */
    private static final DefaultHttpClient    mHttpClient = new DefaultHttpClient();
    //private static BasicHttpContext     mHttpContext;
    private static final CookieStore          mCookieStore = new BasicCookieStore();
    private static final Html.ImageGetter     mIntegrallifeImgGetter;
    private Html.ImageGetter     mGalleryImgGetter;

    static{
        mLock = new Object();
        mHttpClient.setCookieStore(mCookieStore);
        mIntegrallifeImgGetter = new Html.ImageGetter()
        {
             public Drawable getDrawable(String source) {
                 Drawable drawable = null;
                 try {
                     if (null != mHttpClient)
                     {
                         //Log.i("CONTENT_DOWNLOADER", "Img srs = " + source);
                         if (source.contains(" "))
                         {
                             Log.i(logSubsystem, "the URI has a white space, replacing...");
                             source = source.replace(" ", "%20");
                         }
                         if (!source.startsWith("http://"))
                         {
                             source = "http://integrallife.com"+source;
                         }

                        HttpGet request = new HttpGet(source);
                        HttpResponse response = mHttpClient.execute(request);
                        ResponseHandler<Drawable> rh = new ImageResponseHandler();
                        drawable = rh.handleResponse(response);
                        /*
                        drawable = Drawable.createFromStream(response.getEntity().getContent(), null);
                        response.getEntity().consumeContent();
                        // Important
                         if (null != drawable)
                         {
                             int drawHeight = 20;
                             int drawWidth = 20;
                             int width = 20;
                             int height = 20;
                             //int height = display.getWidth();
                             int imgWidth = drawable.getIntrinsicWidth();
                             int imgHeight =  drawable.getIntrinsicHeight();
                             if (width > imgWidth && height > imgHeight)
                             {
                                 drawHeight = imgHeight;
                                 drawWidth = imgWidth;
                             }
                             else if (width < imgWidth)
                             {
                                 drawWidth = width;
                                 drawHeight = (width * imgHeight) / imgWidth;
                             }
                             else if (height < imgHeight)
                             {
                                 drawHeight = height;
                                 drawWidth = (height * imgWidth) / imgHeight;
                             }

                             drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                         }*/
                    }
                     else
                     {
                         Log.e(logSubsystem, "mHttpClient == null!!!!");
                     }
                 } catch (IOException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }
                 catch (IllegalStateException e)
                 {
                     Log.e(logSubsystem, "Huh! mHttpClient = "+mHttpClient);
                     e.printStackTrace();
                 }

                 return drawable;
             }
        };

    }

    public static Html.ImageGetter IlImageGetterGet()
    {
        return mIntegrallifeImgGetter;
    }
    
    public Html.ImageGetter IlImageGalleryGetterGet()
    {
        return mGalleryImgGetter;
    }

    public void requestUpdate()
    {
    	mbUpdate = true;
    }
    
    public DownloadContentTask(DownloadSubscriber listener, int type)
    {
        mListener = listener;
        mType = type;
        mbUpdate = false;
        if (mType == TYPE_GALLERY)
        {
            mGalleryImgGetter =  new Html.ImageGetter()
            {
                 public Drawable getDrawable(String source) {
                     Drawable drawable = null;
                     try {
                         if (null != mHttpClient)
                         {
                             Log.v("CONTENT_DOWNLOADER", "Img srs = " + source);
                             if (source.contains(" "))
                             {
                                 Log.i(logSubsystem, "the URI has a white space, replacing...");
                                 source = source.replace(" ", "%20");
                             }
                             if (!source.startsWith("http://"))
                             {
                                 Log.e(logSubsystem, "Invalid source!!!");
                             }
                             else
                             {
                            	 HttpGet request = new HttpGet(source);
                            	 HttpResponse response = mHttpClient.execute(request);
                            	 drawable = Drawable.createFromStream(response.getEntity().getContent(), null);
                            	 response.getEntity().consumeContent();
                            	 // Important
	                             if (null != drawable)
	                             {
	                                //drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
	                                mListener.Process(drawable, source);
	                             }
	                             else
	                             {
	                                 Log.e(logSubsystem, "IMAGE IS NULL!!!! " + source);
	                             }
                             }
                         }
                         else
                         {
                             Log.e(logSubsystem, "mHttpClient == null!!!!");
                         }
                     } catch (IOException e) {
                         e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                     }
                     catch (IllegalStateException e)
                     {
                         Log.e(logSubsystem, "Huh! mHttpClient = "+mHttpClient);
                         e.printStackTrace();
                     }

                     return drawable;
                 }
            };
        }
    }

    protected CharSequence doInBackground(String... uri)
    {
        //HttpURLConnection urlConnection = null
        synchronized (mLock)
        {
        	if (mbUpdate == false)
        	{
            try {
                //urlConnection = (HttpURLConnection) url.openConnection();
                Log.i(logSubsystem, "Entering the getInputStream");
                //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                HttpGet request = new HttpGet(uri[0]);
                if (null != mHttpClient)
                {
                    HttpResponse response = mHttpClient.execute(request);
                    Log.i(logSubsystem, "After the getInputStream");
                    if (null != response)
                    {
                    	InputStream is = response.getEntity().getContent();
                        readStream(is);
                        is.close();
                    }
                    else
                    {
                        Log.e(logSubsystem, "Error retrieving url: " + uri[0]);
                    }
                    //urlConnection.disconnect();

                    response.getEntity().consumeContent();

                    Log.i("CONTENT_DOWNLOADER","completed");

                    return null;
                }
                else
                {
                    Log.e(logSubsystem, "mHttpClient is null!!!!");
                }

            } catch (IOException e) {
                Log.e(logSubsystem, "got an exception!!!");
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        	}// else just do nothing
        }
        

        return null;
    }

    /**
     * Called after doInBackground.
     * result is the return value from the doInBackground (currently null).
     * Executes in the UI thread.
     * Calls the UpdateFinal() callback method of the DownloadSubscriber.
     */
    protected void onPostExecute(CharSequence result)
    {
    	mListener.Update(result);
        mListener.UpdateFinal();
        mbUpdate = false;
    }

    /**
     * Called as a result of publishProgress() call.
     * This one is executed in the UI thread.
     * Calls the Update() of the DownloadSubscriber.
     */
    protected void onProgressUpdate(String... update) {
         mListener.Update(update[0]);
    }
    
    protected String readStream(InputStream in) {
        try {
            int data_size = 0;
            int data_chunk = 8192;
            byte[] buffer;
            buffer = new byte[data_chunk];
            int bytesread = 0;
            //StringBuilder container = new StringBuilder();
            String ret = null;
            while (bytesread != -1)
            {
                bytesread = in.read(buffer, 0, data_chunk);
                if (bytesread > 0)
                {
                    String data = new String(buffer, 0, bytesread);
                    //container = container.append(data);
                    if (true == mListener.Process(data))
                    {
                        publishProgress("Progress");
                    }
                    data_size += bytesread;
                }
            }

            //if (mType == TYPE_GALLERY)
            //{
            //    Html.fromHtml(container.toString(),mGalleryImgGetter, null);
            //}
            Log.i(logSubsystem, "data_size = " + data_size + " container length");// + container.length());
            mListener.Finalize();

            return ret;
            //Toast.makeText(ArticleView.this, "this much " + bytesread + "was read", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function is intended to extract content from the provided bytes array
     * It would search for certain html tags
     *
    private StringBuilder extractContent(StringBuilder c, String s)
    {
        StringBuilder ret;
        if (null == c) {
            c = new StringBuilder();
        }
        ret = c.append(s);

        return ret;
    }*/

}
