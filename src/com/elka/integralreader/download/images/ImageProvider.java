package com.elka.integralreader.download.images;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;

public final class ImageProvider {
    private final static String tag = ImageProvider.class.getSimpleName(); 
    public static ImageLoader buildLoader(Context context, final int cacheSize) {
        RequestQueue queue = Volley.newRequestQueue(context);
        ImageCache imageCache = new ImageCache() {
            private final LruCache<String, Bitmap> mImageCache = new LruCache<String, Bitmap>(cacheSize);

            @Override
            public Bitmap getBitmap(String key) {
                Bitmap bitmap = mImageCache.get(key);
                if(bitmap != null) {
                    Log.i(tag, "Getting " + key + " from cache");
                } else {
                    Log.i(tag, "Cahe miss!!! " + key);
                }
                return bitmap;
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                Log.i(tag, "Putting " + url + " into cache");
                mImageCache.put(url, bitmap);
            }
        };
        return new ImageLoader(queue, imageCache);
    }
}
