package com.elka.integralreader.download.images;

public interface ImageLoadedListener {
    public void onImageLoaded(String path);
}
