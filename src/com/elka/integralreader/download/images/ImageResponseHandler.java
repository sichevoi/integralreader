package com.elka.integralreader.download.images;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;

import android.graphics.drawable.Drawable;

public class ImageResponseHandler implements ResponseHandler<Drawable> {

	@Override
	public Drawable handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		Drawable drawable = Drawable.createFromStream(response.getEntity().getContent(), null);
        response.getEntity().consumeContent();
        //set bounds to the image
        if (null != drawable)
        {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
		
        return drawable;
	}
}
